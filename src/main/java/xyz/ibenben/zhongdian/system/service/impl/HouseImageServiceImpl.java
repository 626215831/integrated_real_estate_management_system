package xyz.ibenben.zhongdian.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import xyz.ibenben.zhongdian.common.annotation.SystemServiceLog;
import xyz.ibenben.zhongdian.common.constants.Constants;
import xyz.ibenben.zhongdian.common.util.FileUtil;
import xyz.ibenben.zhongdian.system.entity.HouseImage;
import xyz.ibenben.zhongdian.system.entity.enums.ImageTypeEnum;
import xyz.ibenben.zhongdian.system.form.HouseImageForm;
import xyz.ibenben.zhongdian.system.mapper.HouseImageMapper;
import xyz.ibenben.zhongdian.system.service.HouseImageService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * 房屋图像服务实现类
 * 提供了一些基本的服务，如插入记录、更新图像、根据houseId获取列表等方法。
 * 此类提供了所有系统里图像的处理功能
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class HouseImageServiceImpl extends ServiceImpl<HouseImageMapper, HouseImage> implements HouseImageService {
    @Resource
    private HouseImageMapper houseImageMapper;

    /**
     * insert房屋图像信息
     *
     * @param id      参数
     * @param files   参数
     * @param request 参数
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    @SystemServiceLog(description = "insert房屋图像信息")
    public void insert(Long id, List<MultipartFile> files, HttpServletRequest request) {
        for (MultipartFile file : files) {
            //获取地址
            saveInfo(FileUtil.addPhoto(file, request), id);
        }
    }

    /**
     * update房屋图像信息
     *
     * @param id      参数
     * @param files   参数
     * @param request 参数
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    @SystemServiceLog(description = "update房屋图像信息")
    public void update(Long id, List<MultipartFile> files, HttpServletRequest request) {
        for (int i = 0; i < files.size(); i++) {
            MultipartFile file = files.get(i);
            if (file.isEmpty()) {
                continue;
            }
            List<HouseImage> list = findByHouseIdAndSort(id, i);
            if (list != null && !list.isEmpty()) {
                HouseImage image = list.get(0);
                //获取图像地址
                String imageurl = FileUtil.upload(file, image.getHouseImg(), request);
                if (!StringUtils.isBlank(imageurl)) {
                    //如果发现地址，更新操作
                    image.setUpdateId((Long) request.getSession().getAttribute(Constants.SESSIONID));
                    image.setUpdateTime(new Date());
                    image.setHouseImg(imageurl);
                    houseImageMapper.updateById(image);
                }
            } else {
                //没找到记录
                saveInfo(FileUtil.upload(file, null, request), id);
            }
        }
    }

    /**
     * 保存照片信息
     *
     * @param imageurl 参数
     * @param id       参数
     */
    private void saveInfo(String imageurl, Long id) {
        if (!StringUtils.isBlank(imageurl)) {
            //组装实体类
            HouseImage image = new HouseImage();
            image.setHouseId(id);
            image.setHouseImg(imageurl);
            image.setType(ImageTypeEnum.HOUSE);
            //保存新的记录
            this.save(image);
        }
    }

    /**
     * 根据house主键和sort查询记录
     *
     * @param id 参数
     * @param i  参数
     * @return 返回值
     */
    private List<HouseImage> findByHouseIdAndSort(Long id, Integer i) {
        //查询记录
        QueryWrapper<HouseImage> wrapper = new QueryWrapper<>();
        wrapper.eq("house_id", id);
        wrapper.eq("sort", i + 1);
        return houseImageMapper.selectList(wrapper);
    }

    /**
     * findByHouseId房屋图像信息
     *
     * @param houseId 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "findByHouseId房屋图像信息")
    public List<HouseImage> findByHouseId(Long houseId) {
        QueryWrapper<HouseImage> wrapper = new QueryWrapper<>();
        wrapper.eq("house_id", houseId);
        return houseImageMapper.selectList(wrapper);
    }

    @Override
    public IPage<HouseImage> findAll(Page<HouseImage> page, HouseImageForm houseImageForm) {
        return houseImageMapper.findAll(page, houseImageForm);
    }
}
