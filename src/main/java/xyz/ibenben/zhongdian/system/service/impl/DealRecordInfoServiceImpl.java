package xyz.ibenben.zhongdian.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import xyz.ibenben.zhongdian.system.entity.DealRecordInfo;
import xyz.ibenben.zhongdian.system.form.DealRecordInfoForm;
import xyz.ibenben.zhongdian.system.mapper.DealRecordInfoMapper;
import xyz.ibenben.zhongdian.system.service.DealRecordInfoService;

import javax.annotation.Resource;

/**
 * 成交记录信息服务实现类
 * 提供了一些基本的服务，如根据主键查找成交记录信息等方法。
 * 是此系统的主要服务类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class DealRecordInfoServiceImpl extends ServiceImpl<DealRecordInfoMapper, DealRecordInfo> implements DealRecordInfoService {
    @Resource
    private DealRecordInfoMapper dealRecordInfoMapper;

    @Override
    public IPage<DealRecordInfo> findAll(Page<DealRecordInfo> page, DealRecordInfoForm dealRecordInfoForm) {
        return dealRecordInfoMapper.findAll(page, dealRecordInfoForm);
    }

    @Override
    public DealRecordInfo findByKey(Long id) {
        return dealRecordInfoMapper.findByKey(id);
    }
}
