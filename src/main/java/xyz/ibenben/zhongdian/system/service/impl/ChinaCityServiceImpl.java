package xyz.ibenben.zhongdian.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import xyz.ibenben.zhongdian.common.annotation.SystemServiceLog;
import xyz.ibenben.zhongdian.system.entity.ChinaCity;
import xyz.ibenben.zhongdian.system.mapper.ChinaCityMapper;
import xyz.ibenben.zhongdian.system.service.ChinaCityService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 城市服务实现类
 * 此类提供对城市查询的服务，是三级联动里的第二级
 * 提供了一些基本的服务，如通过代码查列表、通过代码查名称等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class ChinaCityServiceImpl extends ServiceImpl<ChinaCityMapper, ChinaCity> implements ChinaCityService {
    @Resource
    private ChinaCityMapper chinaCityMapper;

    /**
     * findListByProvinceCode城市记录
     *
     * @param provinceCode 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "findListByProvinceCode城市记录")
    public List<ChinaCity> findListByProvinceCode(Long provinceCode) {
        QueryWrapper<ChinaCity> wrapper = new QueryWrapper<>();
        wrapper.eq("province_code", provinceCode);
        return chinaCityMapper.selectList(wrapper);
    }

    /**
     * findNameByCode城市记录
     *
     * @param cityCode 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "findNameByCode城市记录")
    public String findNameByCode(Long cityCode) {
        String resultStr = "";
        if (null != cityCode) {
            QueryWrapper<ChinaCity> wrapper = new QueryWrapper<>();
            wrapper.eq("city_code", cityCode);
            ChinaCity result = chinaCityMapper.selectOne(wrapper);
            resultStr = result.getCityName();
        }
        return resultStr;
    }

    @Override
    public List<Long> findCodeByName(String name, Long provinceCode) {
        List<Long> list = new ArrayList<>();
        //返回结果
        if (null != name && !"".equals(name)) {
            //组装实体
            QueryWrapper<ChinaCity> wrapper = new QueryWrapper<>();
            wrapper.eq("city_name", name);
            wrapper.eq("province_code", provinceCode);
            List<ChinaCity> result = chinaCityMapper.selectList(wrapper);
            if (result != null) {
                for (ChinaCity chinaCity : result) {
                    list.add(chinaCity.getCityCode());
                }
            }
        }
        return list;
    }
}
