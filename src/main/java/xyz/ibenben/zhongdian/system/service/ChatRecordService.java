package xyz.ibenben.zhongdian.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import xyz.ibenben.zhongdian.system.entity.ChatRecord;
import xyz.ibenben.zhongdian.system.form.ChatRecordForm;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 聊天记录服务类
 * 提供了一些基本的服务，如获取指定数量的记录列表、保存聊天记录等方法。
 * 是用户在互相聊天时所需要操作的类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface ChatRecordService extends IService<ChatRecord> {
    /**
     * 获取指定数量的记录列表
     *
     * @param offset 参数
     * @param count  参数
     * @param fromId 参数
     * @param toId   参数
     * @param type   参数
     * @return 返回值
     */
    List<ChatRecord> selectByLimit(int offset, int count, Long fromId, Long toId, Integer type);

    /**
     * 保存聊天记录
     *
     * @param record  参数
     * @param request 参数
     * @param id      参数
     */
    int save(ChatRecord record, HttpServletRequest request, Long id);

    /**
     * 根据条件查询列表
     *
     * @param page           分页条件
     * @param chatRecordForm 条件
     * @return 列表
     */
    IPage<ChatRecord> findAll(Page<ChatRecord> page, ChatRecordForm chatRecordForm);

    /**
     * 根据主键获取记录
     *
     * @param id 主键
     * @return 记录
     */
    ChatRecord findByKey(Long id);
}
