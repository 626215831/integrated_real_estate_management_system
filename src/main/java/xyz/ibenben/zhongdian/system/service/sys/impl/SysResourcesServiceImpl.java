package xyz.ibenben.zhongdian.system.service.sys.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import xyz.ibenben.zhongdian.common.annotation.SystemServiceLog;
import xyz.ibenben.zhongdian.system.entity.sys.SysResource;
import xyz.ibenben.zhongdian.system.entity.sys.SysResources;
import xyz.ibenben.zhongdian.system.form.sys.SysResourcesForm;
import xyz.ibenben.zhongdian.system.mapper.sys.SysResourcesMapper;
import xyz.ibenben.zhongdian.system.service.sys.SysResourcesService;

import javax.annotation.Resource;
import java.util.List;

/**
 * 系统资源服务实现类
 * 系统级资源管理所使用的，是权限系统中的一部分
 * 提供了一些基本的服务，如获取用户资源、查询资源列表等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service("sysResourcesService")
public class SysResourcesServiceImpl extends ServiceImpl<SysResourcesMapper, SysResources> implements SysResourcesService {
    @Resource
    private SysResourcesMapper sysResourcesMapper;

    /**
     * loadUserResources资源记录
     *
     * @param userId 用户主键
     * @return 返回值
     */
    @Override
    @Cacheable(value = "resources")
    @SystemServiceLog(description = "loadUserResources资源记录")
    public List<SysResources> loadUserResources(Long userId) {
        System.out.println("print service");
        return sysResourcesMapper.loadUserResources(userId);
    }

    /**
     * queryResourcesListWithSelected资源记录
     *
     * @param rid 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "queryResourcesListWithSelected资源记录")
    public List<SysResource> queryResourcesListWithSelected(Long rid) {
        return sysResourcesMapper.queryResourcesListWithSelected(rid);
    }

    /**
     * selectByName资源记录
     *
     * @param name 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "selectByName资源记录")
    public SysResources selectByName(String name) {
        SysResources sysResources = null;
        QueryWrapper<SysResources> wrapper = new QueryWrapper<>();
        wrapper.eq("name", name);
        List<SysResources> resourcesList = sysResourcesMapper.selectList(wrapper);
        if (!resourcesList.isEmpty()) {
            sysResources = resourcesList.get(0);
        }
        return sysResources;
    }

    /**
     * findNameByResurl资源记录
     *
     * @param uri 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "findNameByResurl资源记录")
    public SysResources findNameByResurl(String uri) {
        SysResources sysResources = null;
        QueryWrapper<SysResources> wrapper = new QueryWrapper<>();
        wrapper.eq("res_url", uri);
        List<SysResources> resourcesList = sysResourcesMapper.selectList(wrapper);
        if (!resourcesList.isEmpty()) {
            sysResources = resourcesList.get(0);
        }
        return sysResources;
    }

    /**
     * findByType资源记录
     *
     * @param type 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "findByType资源记录")
    public List<SysResources> findByType(int type) {
        QueryWrapper<SysResources> wrapper = new QueryWrapper<>();
        wrapper.eq("type", type);
        return sysResourcesMapper.selectList(wrapper);
    }

    @Override
    public IPage<SysResources> findAll(Page<SysResources> page, SysResourcesForm sysResourcesForm) {
        return sysResourcesMapper.findAll(page, sysResourcesForm);
    }
}
