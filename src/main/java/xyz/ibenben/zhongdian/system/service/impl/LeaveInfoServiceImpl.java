package xyz.ibenben.zhongdian.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.ibenben.zhongdian.common.annotation.SystemServiceLog;
import xyz.ibenben.zhongdian.system.entity.LeaveInfo;
import xyz.ibenben.zhongdian.system.form.LeaveInfoForm;
import xyz.ibenben.zhongdian.system.mapper.LeaveInfoMapper;
import xyz.ibenben.zhongdian.system.service.LeaveInfoService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * 请假申请记录服务实现类
 * 提供了一些基本的服务，如获取指定数量的记录列表、保存请假申请记录等方法。
 * 是用户在请假时所需要操作的类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class LeaveInfoServiceImpl extends ServiceImpl<LeaveInfoMapper, LeaveInfo> implements LeaveInfoService {
    @Resource
    private LeaveInfoMapper leaveInfoMapper;


    /**
     * save请假申请记录
     *
     * @param entity  参数
     * @param request 参数
     * @param id      参数
     * @return 返回值
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    @SystemServiceLog(description = "save聊天记录")
    public int save(LeaveInfo entity, HttpServletRequest request, Long id) {
        entity.setCreateId(id);
        entity.setCreateTime(new Date());
        entity.setUpdateId(id);
        entity.setUpdateTime(new Date());
        return leaveInfoMapper.insert(entity);
    }

    @Override
    public IPage<LeaveInfo> findAll(Page<LeaveInfo> page, LeaveInfoForm leaveInfoForm) {
        return leaveInfoMapper.findAll(page, leaveInfoForm);
    }

}
