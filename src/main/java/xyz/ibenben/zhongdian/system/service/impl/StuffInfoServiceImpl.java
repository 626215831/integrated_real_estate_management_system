package xyz.ibenben.zhongdian.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import xyz.ibenben.zhongdian.system.entity.StuffInfo;
import xyz.ibenben.zhongdian.system.form.StuffInfoForm;
import xyz.ibenben.zhongdian.system.mapper.StuffInfoMapper;
import xyz.ibenben.zhongdian.system.service.StuffInfoService;

import javax.annotation.Resource;
import java.util.List;

/**
 * 租赁物品信息服务实现类
 * 提供了一些基本的服务，如根据主键查找租赁物品信息等方法。
 * 是此系统的主要服务类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class StuffInfoServiceImpl extends ServiceImpl<StuffInfoMapper, StuffInfo> implements StuffInfoService {
    @Resource
    private StuffInfoMapper stuffInfoMapper;

    @Override
    public List<StuffInfo> selectByRentingHouseId(Long rentingId) {
        QueryWrapper<StuffInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("renting_id", rentingId);
        return stuffInfoMapper.selectList(wrapper);
    }

    @Override
    public IPage<StuffInfo> findAll(Page<StuffInfo> page, StuffInfoForm stuffInfoForm) {
        return stuffInfoMapper.findAll(page, stuffInfoForm);
    }

    @Override
    public StuffInfo findByKey(Long id) {
        return stuffInfoMapper.findByKey(id);
    }
}
