package xyz.ibenben.zhongdian.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import xyz.ibenben.zhongdian.system.entity.StuffInfo;
import xyz.ibenben.zhongdian.system.form.StuffInfoForm;

import java.util.List;

/**
 * 租赁物品信息服务类
 * 提供了一些基本的服务，如根据主键查找租赁物品信息
 * 是此系统的主要服务类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface StuffInfoService extends IService<StuffInfo> {
    /**
     * 根据租赁信息查找对应的物品列表
     *
     * @param id 租赁信息主键
     * @return 物品列表
     */
    List<StuffInfo> selectByRentingHouseId(Long id);

    /**
     * 根据条件查询列表
     *
     * @param page          分页参数
     * @param stuffInfoForm 条件
     * @return 列表
     */
    IPage<StuffInfo> findAll(Page<StuffInfo> page, StuffInfoForm stuffInfoForm);

    /**
     * 根据主键获取记录
     *
     * @param id 主键
     * @return 记录
     */
    StuffInfo findByKey(Long id);
}
