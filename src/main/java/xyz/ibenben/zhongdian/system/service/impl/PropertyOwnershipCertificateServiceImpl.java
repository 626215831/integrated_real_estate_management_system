package xyz.ibenben.zhongdian.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import xyz.ibenben.zhongdian.system.entity.PropertyOwnershipCertificate;
import xyz.ibenben.zhongdian.system.form.PropertyOwnershipCertificateForm;
import xyz.ibenben.zhongdian.system.mapper.PropertyOwnershipCertificateMapper;
import xyz.ibenben.zhongdian.system.service.PropertyOwnershipCertificateService;

import javax.annotation.Resource;

/**
 * 产权服务实现类
 * 提供了一些基本的服务
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class PropertyOwnershipCertificateServiceImpl extends ServiceImpl<PropertyOwnershipCertificateMapper, PropertyOwnershipCertificate>
        implements PropertyOwnershipCertificateService {
    @Resource
    private PropertyOwnershipCertificateMapper propertyOwnershipCertificateMapper;

    @Override
    public IPage<PropertyOwnershipCertificate> findAll(Page<PropertyOwnershipCertificate> page,
                                                       PropertyOwnershipCertificateForm propertyOwnershipCertificateForm) {
        return propertyOwnershipCertificateMapper.findAll(page, propertyOwnershipCertificateForm);
    }
}
