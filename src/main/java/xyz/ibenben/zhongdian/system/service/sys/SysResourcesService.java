package xyz.ibenben.zhongdian.system.service.sys;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import xyz.ibenben.zhongdian.system.entity.sys.SysResource;
import xyz.ibenben.zhongdian.system.entity.sys.SysResources;
import xyz.ibenben.zhongdian.system.form.sys.SysResourcesForm;

import java.util.List;

/**
 * 系统资源服务类
 * 系统级资源管理所使用的，是权限系统中的一部分
 * 提供了一些基本的服务，如获取用户资源、查询资源列表等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface SysResourcesService extends IService<SysResources> {
    /**
     * 获取用户资源
     *
     * @param userId 用户主键
     * @return 返回值
     */
    List<SysResources> loadUserResources(Long userId);

    /**
     * 查询资源列表
     *
     * @param rid 参数
     * @return 返回值
     */
    List<SysResource> queryResourcesListWithSelected(Long rid);

    /**
     * 根据资源名称查询资源是否存在
     *
     * @param name 参数
     * @return 返回值
     */
    SysResources selectByName(String name);

    /**
     * 根据链接查名字
     *
     * @param uri 参数
     * @return 返回值
     */
    SysResources findNameByResurl(String uri);

    /**
     * 根据类型获取记录
     *
     * @param i 参数
     * @return 返回值
     */
    List<SysResources> findByType(int i);

    /**
     * 根据条件查询列表
     *
     * @param page             分页参数
     * @param sysResourcesForm 条件
     * @return 列表
     */
    IPage<SysResources> findAll(Page<SysResources> page, SysResourcesForm sysResourcesForm);
}
