package xyz.ibenben.zhongdian.system.service.mongodb;

import xyz.ibenben.zhongdian.system.entity.mongodb.MongoChinaRegion;

import java.util.List;

/**
 * 区县服务类
 * 此类提供对区县查询的服务，是三级联动里的最后一级
 * 提供了一些基本的服务，如通过代码查列表、通过代码查名称等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface MongoChinaRegionService extends MongoService<MongoChinaRegion> {
    /**
     * 通过代码查列表
     *
     * @param code 参数
     * @return 返回值
     */
    List<MongoChinaRegion> findListByCityCode(Long code);

    /**
     * 通过代码查名称
     *
     * @param code 参数
     * @return 返回值
     */
    String findNameByCode(Long code);
}
