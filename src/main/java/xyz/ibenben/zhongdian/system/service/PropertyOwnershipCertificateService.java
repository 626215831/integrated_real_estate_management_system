package xyz.ibenben.zhongdian.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import xyz.ibenben.zhongdian.system.entity.PropertyOwnershipCertificate;
import xyz.ibenben.zhongdian.system.form.PropertyOwnershipCertificateForm;

/**
 * 产权服务类
 * 提供了一些基本的服务
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface PropertyOwnershipCertificateService extends IService<PropertyOwnershipCertificate> {
    /**
     * 根据条件查询列表
     *
     * @param page                             分页参数
     * @param propertyOwnershipCertificateForm 条件
     * @return 列表
     */
    IPage<PropertyOwnershipCertificate> findAll(Page<PropertyOwnershipCertificate> page,
                                                PropertyOwnershipCertificateForm propertyOwnershipCertificateForm);
}
