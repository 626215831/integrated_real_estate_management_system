package xyz.ibenben.zhongdian.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import xyz.ibenben.zhongdian.system.entity.HouseBooking;
import xyz.ibenben.zhongdian.system.form.HouseBookingForm;

import java.util.List;

/**
 * 房屋预售服务类
 * 提供了一些基本的服务，如根据名称查记录、获取记录带图像等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface HouseBookingService extends IService<HouseBooking> {
    /**
     * 根据名称查记录
     *
     * @param name 参数
     * @return 返回值
     */
    List<HouseBooking> findByName(String name);

    /**
     * 获取记录带图像
     *
     * @param id 参数
     * @return 返回值
     */
    HouseBooking findOneWithImage(Long id);

    /**
     * 根据条件查询列表
     *
     * @param page             分页参数
     * @param houseBookingForm 条件
     * @return 列表
     */
    IPage<HouseBooking> findAll(Page<HouseBooking> page, HouseBookingForm houseBookingForm);
}
