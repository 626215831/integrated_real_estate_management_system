package xyz.ibenben.zhongdian.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import xyz.ibenben.zhongdian.system.entity.Reminder;
import xyz.ibenben.zhongdian.system.form.ReminderForm;

import java.util.Date;
import java.util.List;

/**
 * 系统提醒服务类
 * 提供了一些基本的服务，如获根据条件查询列表等方法。
 * 是用户在系统提醒时所需要操作的类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface ReminderService extends IService<Reminder> {

    /**
     * 根据条件查询列表
     *
     * @param page         分页参数
     * @param reminderForm 条件
     * @return 列表
     */
    IPage<Reminder> findAll(Page<Reminder> page, ReminderForm reminderForm);

    /**
     * 根据主键获取记录
     *
     * @param id 主键
     * @return 记录
     */
    Reminder findByKey(Long id);

    /**
     * 根据时间获取记录列表
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return 记录列表
     */
    List<Reminder> findByTime(Date startTime, Date endTime);
}
