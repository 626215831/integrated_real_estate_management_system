package xyz.ibenben.zhongdian.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.ibenben.zhongdian.system.entity.Log;

/**
 * 日志服务类
 * 提供了一些基本的服务
 * 记录日志到数据库
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface LogService extends IService<Log> {
    //日志
}
