package xyz.ibenben.zhongdian.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.ibenben.zhongdian.system.entity.ChinaProvince;

import java.util.List;

/**
 * 省市服务类
 * 此类提供对省市查询的服务，是三级联动里的第一级
 * 提供了一些基本的服务，如通过代码查列表等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface ChinaProvinceService extends IService<ChinaProvince> {
    /**
     * 通过代码查名称
     *
     * @param code 参数
     * @return 返回值
     */
    String findNameByCode(Long code);

    /**
     * 通过名称找代码
     *
     * @param name 名称
     * @return 列表
     */
    List<Long> findCodeByName(String name);
}
