package xyz.ibenben.zhongdian.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import xyz.ibenben.zhongdian.system.entity.CompanyInfo;
import xyz.ibenben.zhongdian.system.form.CompanyInfoForm;

import java.util.List;

/**
 * 公司信息服务类
 * 提供了一些基本的服务，如根据主键查找公司信息
 * 是此系统的主要服务类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface CompanyInfoService extends IService<CompanyInfo> {
    /**
     * 根据名称查找公司信息
     *
     * @param companyName 公司名称
     * @return 公司信息列表
     */
    List<CompanyInfo> findByCompanyName(String companyName);

    /**
     * 根据主键查找公司信息
     *
     * @param id 公司主键
     * @return 公司信息
     */
    CompanyInfo findByKey(Long id);

    /**
     * 根据条件查询列表
     *
     * @param page            分页参数
     * @param companyInfoForm 条件
     * @return 列表
     */
    IPage<CompanyInfo> findAll(Page<CompanyInfo> page, CompanyInfoForm companyInfoForm);
}
