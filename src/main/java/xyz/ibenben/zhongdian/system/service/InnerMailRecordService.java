package xyz.ibenben.zhongdian.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.ibenben.zhongdian.system.entity.InnerMailRecord;

import java.io.Serializable;

/**
 * 站内信记录服务类
 * 提供了一些基本的服务，如根据公告Id和用户主键查找记录、
 * 根据用户获取未读消息数量、根据站内信主键删除记录等方法.
 * 主要用于用户之间通信使用此类来维护
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface InnerMailRecordService extends IService<InnerMailRecord> {
    /**
     * 根据公告Id和用户主键查找记录
     *
     * @param id     参数
     * @param userId 参数
     * @return 返回值
     */
    InnerMailRecord selectByMailIdAndUserId(Long id, Long userId);

    /**
     * 根据站内信主键删除记录
     *
     * @param key 参数
     * @return 返回值
     */
    boolean deleteByInnerMailKey(Serializable key);
}
