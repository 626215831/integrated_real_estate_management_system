package xyz.ibenben.zhongdian.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import xyz.ibenben.zhongdian.system.entity.CompanyInfo;
import xyz.ibenben.zhongdian.system.form.CompanyInfoForm;
import xyz.ibenben.zhongdian.system.mapper.CompanyInfoMapper;
import xyz.ibenben.zhongdian.system.service.CompanyInfoService;

import javax.annotation.Resource;
import java.util.List;

/**
 * 公司信息服务实现类
 * 提供了一些基本的服务，如根据主键查找公司信息等方法。
 * 是此系统的主要服务类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class CompanyInfoServiceImpl extends ServiceImpl<CompanyInfoMapper, CompanyInfo> implements CompanyInfoService {
    @Resource
    private CompanyInfoMapper companyInfoMapper;

    @Override
    public List<CompanyInfo> findByCompanyName(String companyName) {
        //通过房屋名称获取房屋信息列表
        QueryWrapper<CompanyInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("company_name", companyName);
        return companyInfoMapper.selectList(wrapper);
    }

    @Override
    public CompanyInfo findByKey(Long id) {
        return companyInfoMapper.findByKey(id);
    }

    @Override
    public IPage<CompanyInfo> findAll(Page<CompanyInfo> page, CompanyInfoForm companyInfoForm) {
        return companyInfoMapper.findAll(page, companyInfoForm);
    }
}
