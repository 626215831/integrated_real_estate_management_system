package xyz.ibenben.zhongdian.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.ibenben.zhongdian.common.annotation.SystemServiceLog;
import xyz.ibenben.zhongdian.system.entity.InnerMailRecord;
import xyz.ibenben.zhongdian.system.mapper.InnerMailRecordMapper;
import xyz.ibenben.zhongdian.system.service.InnerMailRecordService;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 站内信服务实现类
 * 提供了一些基本的服务，如根据公告Id和用户主键查找记录、
 * 根据用户获取未读消息数量、根据站内信主键删除记录等方法.
 * 主要用于用户之间通信使用此类来维护
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service("innerMailRecordService")
public class InnerMailRecordServiceImpl extends ServiceImpl<InnerMailRecordMapper, InnerMailRecord> implements InnerMailRecordService {
    @Resource
    private InnerMailRecordMapper innerMailRecordMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    @SystemServiceLog(description = "deleteByInnerMailKey站内信记录")
    public boolean deleteByInnerMailKey(Serializable key) {
        boolean result = false;
        //获取条件imId = key的所有记录列表
        List<InnerMailRecord> list = innerMailRecordMapper.selectList(getExample(key, null));
        if (list != null && !list.isEmpty()) {
            //逻辑删除
            InnerMailRecord entity = list.get(0);
            entity.setDelFlag(0);
            entity.setDelId((Long) SecurityUtils.getSubject().getSession().getAttribute("currentUserId"));
            entity.setDelTime(new Date());
            result = this.updateById(entity);
        }
        return result;
    }

    @Override
    @SystemServiceLog(description = "selectByMailIdAndUserId站内信记录")
    public InnerMailRecord selectByMailIdAndUserId(Long id, Long userId) {
        //通过发送者和接收者获取记录
        List<InnerMailRecord> list = innerMailRecordMapper.selectList(getExample(userId, id));
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }

    /**
     * 获取条件实例
     *
     * @param toId 接收者
     * @param imId 发送者
     * @return 条件实例
     */
    private QueryWrapper<InnerMailRecord> getExample(Object toId, Long imId) {
        QueryWrapper<InnerMailRecord> wrapper = new QueryWrapper<>();
        //接收者
        if (toId != null) {
            wrapper.eq("to_id", toId);
        }
        //发送者
        if (imId != null) {
            wrapper.eq("im_id", imId);
        }
        return wrapper;
    }

}
