package xyz.ibenben.zhongdian.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.ibenben.zhongdian.common.annotation.SystemServiceLog;
import xyz.ibenben.zhongdian.system.entity.HouseBooking;
import xyz.ibenben.zhongdian.system.entity.HouseImage;
import xyz.ibenben.zhongdian.system.entity.HouseInfo;
import xyz.ibenben.zhongdian.system.entity.enums.HouseTypeEnum;
import xyz.ibenben.zhongdian.system.form.HouseBookingForm;
import xyz.ibenben.zhongdian.system.mapper.HouseBookingMapper;
import xyz.ibenben.zhongdian.system.service.HouseBookingService;
import xyz.ibenben.zhongdian.system.service.HouseImageService;
import xyz.ibenben.zhongdian.system.service.HouseInfoService;

import javax.annotation.Resource;
import java.util.List;

/**
 * 房屋预售服务实现类
 * 提供了一些基本的服务，如根据名称查记录、获取记录带图像等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class HouseBookingServiceImpl extends ServiceImpl<HouseBookingMapper, HouseBooking> implements HouseBookingService {
    @Resource
    private HouseBookingMapper houseBookingMapper;
    @Resource
    private HouseInfoService houseInfoService;
    @Resource
    private HouseImageService houseImageService;

    /**
     * save预售信息
     *
     * @param entity 实体
     * @return 是否保存成功
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    @SystemServiceLog(description = "save预售信息")
    public boolean save(HouseBooking entity) {
        //组装房屋实体
        HouseInfo info = entity.getInfo();
        info.setHouseName(entity.getName());
        info.setAddress(entity.getAddress());
        info.setProvince(entity.getProvince().toString());
        info.setCity(entity.getCity().toString());
        info.setRegion(entity.getRegion().toString());
        info.setType(HouseTypeEnum.BOOKING);
        //保存房屋信息
        houseInfoService.save(info);
        //保存预售信息
        entity.setHouseId(info.getId());
        entity.setHouseArea(info.getHouseArea());
        return this.save(entity);
    }

    /**
     * updateAll预售信息
     *
     * @param entity 实体
     * @return 是否更新成功
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    @SystemServiceLog(description = "updateAll预售信息")
    public boolean updateById(HouseBooking entity) {
        //组装房屋信息
        HouseInfo info = entity.getInfo();
        info.setHouseName(entity.getName());
        info.setAddress(entity.getAddress());
        info.setProvince(entity.getProvince().toString());
        info.setCity(entity.getCity().toString());
        info.setRegion(entity.getRegion().toString());
        //更新房屋信息
        houseInfoService.updateById(info);
        //更新预售信息
        entity.setHouseArea(info.getHouseArea());
        return this.updateById(entity);
    }

    /**
     * findByName预售信息
     *
     * @param name 预售房名称
     * @return 预售房列表
     */
    @Override
    @SystemServiceLog(description = "findByName预售信息")
    public List<HouseBooking> findByName(String name) {
        //根据预售名称查询预售列表
        QueryWrapper<HouseBooking> wrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(name)) {
            wrapper.eq("name", name);
        }
        return houseBookingMapper.selectList(wrapper);
    }

    /**
     * findOneWithImage预售信息
     *
     * @param id 房屋主键
     * @return 预售信息
     */
    @Override
    @SystemServiceLog(description = "findOneWithImage预售信息")
    public HouseBooking findOneWithImage(Long id) {
        HouseBooking book = houseBookingMapper.selectById(id);
        if (book.getHouseId() != null) {
            //为预售增加房屋信息
            HouseInfo info = houseInfoService.findOneWithImage(book.getHouseId());
            book.setInfo(info);
        }

        List<HouseImage> list = houseImageService.findByHouseId(id);
        if (list != null && !list.isEmpty()) {
            //为预售增加图像信息列表
            book.setList(list);
        }
        return book;
    }

    @Override
    public IPage<HouseBooking> findAll(Page<HouseBooking> page, HouseBookingForm houseBookingForm) {
        return houseBookingMapper.findAll(page, houseBookingForm);
    }
}
