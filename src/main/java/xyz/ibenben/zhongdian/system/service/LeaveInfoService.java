package xyz.ibenben.zhongdian.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import xyz.ibenben.zhongdian.system.entity.LeaveInfo;
import xyz.ibenben.zhongdian.system.form.LeaveInfoForm;

import javax.servlet.http.HttpServletRequest;

/**
 * 请假记录服务类
 * 提供了一些基本的服务，如获取指定数量的记录列表、保存请假申请记录等方法。
 * 是用户在请假时所需要操作的类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface LeaveInfoService extends IService<LeaveInfo> {

    /**
     * 保存聊天记录
     *
     * @param record  参数
     * @param request 参数
     * @param id      参数
     */
    int save(LeaveInfo record, HttpServletRequest request, Long id);

    /**
     * 根据条件查询列表
     *
     * @param page          分页条件
     * @param leaveInfoForm 条件
     * @return 列表
     */
    IPage<LeaveInfo> findAll(Page<LeaveInfo> page, LeaveInfoForm leaveInfoForm);

}
