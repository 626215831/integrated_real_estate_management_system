package xyz.ibenben.zhongdian.system.service.elasticsearch;

import xyz.ibenben.zhongdian.system.entity.HouseInfo;
import xyz.ibenben.zhongdian.system.entity.elasticsearch.ElasticsearchHouseInfo;

import java.util.List;
import java.util.Map;

/**
 * 房屋信息服务类
 * 提供了一些基本的服务，如根据主键查找房屋信息及图片、
 * 根据房屋名称查询记录、导出列表等方法。
 * 是此系统的主要服务类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface ElasticsearchHouseInfoService extends ElasticsearchService<ElasticsearchHouseInfo> {
    /**
     * 根据主键查找房屋信息及图片
     *
     * @param id 参数
     * @return 返回值
     */
    ElasticsearchHouseInfo findOneWithImage(Long id);

    /**
     * 根据房屋名称查询记录
     *
     * @param houseName 参数
     * @return 返回值
     */
    List<ElasticsearchHouseInfo> findByHouseName(String houseName);

    /**
     * 根据用户主键和类型查找房屋信息
     *
     * @param id   参数
     * @param type 参数
     * @return 返回值
     */
    List<ElasticsearchHouseInfo> findByCreateIdAndType(Long id, int type);

    /**
     * 获取房屋饼图信息
     *
     * @return 返回值
     */
    Map<String, Object> getHouseInfoList();

    /**
     * 根据原型保存到查询数据库中
     *
     * @param houseInfo  原型实体类
     * @param entityName 实体类名称
     */
    void saveEntity(HouseInfo houseInfo, String entityName);

    /**
     * 根据原型更新到查询数据库中
     *
     * @param houseInfo  原型实体类
     * @param entityName 实体类名称
     */
    void updateEntity(HouseInfo houseInfo, String entityName);
}
