package xyz.ibenben.zhongdian.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;
import xyz.ibenben.zhongdian.system.entity.HouseImage;
import xyz.ibenben.zhongdian.system.form.HouseImageForm;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 房屋图像服务类
 * 提供了一些基本的服务，如插入记录、更新图像、根据houseId获取列表等方法。
 * 此类提供了所有系统里图像的处理功能
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface HouseImageService extends IService<HouseImage> {
    /**
     * 插入记录
     *
     * @param id      参数
     * @param files   参数
     * @param request 参数
     */
    void insert(Long id, List<MultipartFile> files, HttpServletRequest request);

    /**
     * 更新图像
     *
     * @param id      参数
     * @param files   参数
     * @param request 参数
     */
    void update(Long id, List<MultipartFile> files, HttpServletRequest request);

    /**
     * 根据houseId获取列表
     *
     * @param houseId 参数
     * @return 返回值
     */
    List<HouseImage> findByHouseId(Long houseId);

    /**
     * 根据条件查询列表
     *
     * @param page           分页参数
     * @param houseImageForm 条件
     * @return 列表
     */
    IPage<HouseImage> findAll(Page<HouseImage> page, HouseImageForm houseImageForm);
}
