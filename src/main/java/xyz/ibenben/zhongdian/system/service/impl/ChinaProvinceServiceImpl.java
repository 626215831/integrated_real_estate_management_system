package xyz.ibenben.zhongdian.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import xyz.ibenben.zhongdian.common.annotation.SystemServiceLog;
import xyz.ibenben.zhongdian.system.entity.ChinaProvince;
import xyz.ibenben.zhongdian.system.mapper.ChinaProvinceMapper;
import xyz.ibenben.zhongdian.system.service.ChinaProvinceService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 省市服务实现类
 * 此类提供对省市查询的服务，是三级联动里的第一级
 * 提供了一些基本的服务，如通过代码查列表等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class ChinaProvinceServiceImpl extends ServiceImpl<ChinaProvinceMapper, ChinaProvince> implements ChinaProvinceService {
    @Resource
    private ChinaProvinceMapper chinaProvinceMapper;

    /**
     * findNameByCode省份记录
     *
     * @param provinceCode 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "findNameByCode省份记录")
    public String findNameByCode(Long provinceCode) {
        //返回结果
        if (null != provinceCode) {
            //组装实体
            QueryWrapper<ChinaProvince> wrapper = new QueryWrapper<>();
            wrapper.eq("province_code", provinceCode);
            ChinaProvince result = chinaProvinceMapper.selectOne(wrapper);
            return result.getProvinceName();
        }
        return "";
    }

    /**
     * 通过名称找代码
     *
     * @param name 名称
     * @return 返回值
     */
    @Override
    public List<Long> findCodeByName(String name) {
        List<Long> list = new ArrayList<>();
        //返回结果
        if (null != name && !"".equals(name)) {
            //组装实体
            QueryWrapper<ChinaProvince> wrapper = new QueryWrapper<>();
            wrapper.eq("province_name", name);
            List<ChinaProvince> result = chinaProvinceMapper.selectList(wrapper);
            if (result != null) {
                for (ChinaProvince chinaProvince : result) {
                    list.add(chinaProvince.getProvinceCode());
                }
            }
        }
        return list;
    }
}
