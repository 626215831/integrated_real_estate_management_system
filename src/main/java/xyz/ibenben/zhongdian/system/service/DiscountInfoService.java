package xyz.ibenben.zhongdian.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import xyz.ibenben.zhongdian.system.entity.DiscountInfo;
import xyz.ibenben.zhongdian.system.form.DiscountInfoForm;

/**
 * 折扣信息服务类
 * 提供了一些基本的服务，如根据主键查找折扣信息
 * 是此系统的主要服务类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface DiscountInfoService extends IService<DiscountInfo> {

    /**
     * 根据条件查询列表
     *
     * @param page             分页参数
     * @param discountInfoForm 条件
     * @return 列表
     */
    IPage<DiscountInfo> findAll(Page<DiscountInfo> page, DiscountInfoForm discountInfoForm);
}
