package xyz.ibenben.zhongdian.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import xyz.ibenben.zhongdian.system.entity.HouseLocation;
import xyz.ibenben.zhongdian.system.mapper.HouseLocationMapper;
import xyz.ibenben.zhongdian.system.service.HouseLocationService;

import javax.annotation.Resource;

/**
 * 房屋位置服务实现类
 * 提供了一些基本的服务，如根据主键查找房屋位置
 * 是此系统的主要服务类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class HouseLocationServiceImpl extends ServiceImpl<HouseLocationMapper, HouseLocation> implements HouseLocationService {
    @Resource
    private HouseLocationMapper houseLocationMapper;

    @Override
    public HouseLocation selectByHouseId(Long houseId) {
        QueryWrapper<HouseLocation> wrapper = new QueryWrapper<>();
        wrapper.eq("house_id", houseId);
        return houseLocationMapper.selectOne(wrapper);
    }
}
