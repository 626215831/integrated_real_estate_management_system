package xyz.ibenben.zhongdian.system.service.mongodb;

import java.util.List;

/**
 * @author chenjian
 */
public interface MongoService<T> {
    /**
     * 根据主键获取对象
     *
     * @param key 参数
     * @return 返回值
     */
    T selectByKey(Object key);

    /**
     * 保存对象
     *
     * @param entity 参数
     * @return 返回值
     */
    void save(T entity);

    /**
     * 删除对象
     *
     * @param key 参数
     * @return 返回值
     */
    void delete(Object key);

    /**
     * 更新所有对象
     *
     * @param entity 参数
     * @return 返回值
     */
    void updateAll(T entity);

    /**
     * 更新所有非空对象
     *
     * @param entity 参数
     * @return 返回值
     */
    void updateNotNull(T entity);

    /**
     * 获取所有对象
     *
     * @return 返回值
     */
    List<T> selectAll();

    /**
     * 批量保存數據
     *
     * @param list
     */
    void saveBatch(List<T> list);
}
