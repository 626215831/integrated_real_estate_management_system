package xyz.ibenben.zhongdian.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.ibenben.zhongdian.system.entity.ChinaRegion;

import java.util.List;

/**
 * 区县服务类
 * 此类提供对区县查询的服务，是三级联动里的最后一级
 * 提供了一些基本的服务，如通过代码查列表、通过代码查名称等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface ChinaRegionService extends IService<ChinaRegion> {
    /**
     * 通过代码查列表
     *
     * @param code 参数
     * @return 返回值
     */
    List<ChinaRegion> findListByCityCode(Long code);

    /**
     * 通过代码查名称
     *
     * @param code 参数
     * @return 返回值
     */
    String findNameByCode(Long code);

    /**
     * 根据名称和城市编码获取区县code
     *
     * @param region   名称
     * @param cityCode 城市编码
     * @return 区县code
     */
    List<Long> findCodeByName(String region, Long cityCode);
}
