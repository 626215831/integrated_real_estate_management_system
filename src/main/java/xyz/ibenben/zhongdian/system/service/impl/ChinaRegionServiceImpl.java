package xyz.ibenben.zhongdian.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import xyz.ibenben.zhongdian.common.annotation.SystemServiceLog;
import xyz.ibenben.zhongdian.system.entity.ChinaRegion;
import xyz.ibenben.zhongdian.system.mapper.ChinaRegionMapper;
import xyz.ibenben.zhongdian.system.service.ChinaRegionService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 区县服务实现类
 * 此类提供对区县查询的服务，是三级联动里的最后一级
 * 提供了一些基本的服务，如通过代码查列表、通过代码查名称等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class ChinaRegionServiceImpl extends ServiceImpl<ChinaRegionMapper, ChinaRegion> implements ChinaRegionService {
    @Resource
    private ChinaRegionMapper chinaRegionMapper;

    /**
     * findListByCityCode区域记录
     *
     * @param cityCode 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "findListByCityCode区域记录")
    public List<ChinaRegion> findListByCityCode(Long cityCode) {
        QueryWrapper<ChinaRegion> wrapper = new QueryWrapper<>();
        wrapper.eq("city_code", cityCode);
        return chinaRegionMapper.selectList(wrapper);
    }

    /**
     * findNameByCode区域记录
     *
     * @param regionCode 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "findNameByCode区域记录")
    public String findNameByCode(Long regionCode) {
        String resultStr = "";
        if (null != regionCode) {
            QueryWrapper<ChinaRegion> wrapper = new QueryWrapper<>();
            wrapper.eq("region_code", regionCode);
            ChinaRegion result = chinaRegionMapper.selectOne(wrapper);
            resultStr = result.getRegionName();
        }
        return resultStr;
    }

    @Override
    public List<Long> findCodeByName(String name, Long cityCode) {
        List<Long> list = new ArrayList<>();
        //返回结果
        if (null != name && !"".equals(name)) {
            //组装实体
            QueryWrapper<ChinaRegion> wrapper = new QueryWrapper<>();
            wrapper.eq("region_name", name);
            wrapper.eq("city_code", cityCode);
            List<ChinaRegion> result = chinaRegionMapper.selectList(wrapper);
            if (result != null) {
                for (ChinaRegion chinaRegion : result) {
                    list.add(chinaRegion.getRegionCode());
                }
            }
        }
        return list;
    }
}
