package xyz.ibenben.zhongdian.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.ibenben.zhongdian.common.annotation.SystemServiceLog;
import xyz.ibenben.zhongdian.system.entity.ChatRecord;
import xyz.ibenben.zhongdian.system.form.ChatRecordForm;
import xyz.ibenben.zhongdian.system.mapper.ChatRecordMapper;
import xyz.ibenben.zhongdian.system.service.ChatRecordService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 聊天记录服务实现类
 * 提供了一些基本的服务，如获取指定数量的记录列表、保存聊天记录等方法。
 * 是用户在互相聊天时所需要操作的类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class ChatRecordServiceImpl extends ServiceImpl<ChatRecordMapper, ChatRecord> implements ChatRecordService {
    @Resource
    private ChatRecordMapper chatRecordMapper;

    /**
     * selectByLimit聊天记录
     *
     * @param offset 参数
     * @param count  参数
     * @param fromId 参数
     * @param toId   参数
     * @param type   参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "selectByLimit聊天记录")
    public List<ChatRecord> selectByLimit(int offset, int count, Long fromId, Long toId, Integer type) {
        Map<String, Object> map = new HashMap<>(5);
        map.put("offset", offset);
        map.put("count", count);
        map.put("fromId", fromId);
        map.put("toId", toId);
        map.put("type", type);
        return chatRecordMapper.selectByLimit(map);
    }

    /**
     * save聊天记录
     *
     * @param entity  参数
     * @param request 参数
     * @param id      参数
     * @return 返回值
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    @SystemServiceLog(description = "save聊天记录")
    public int save(ChatRecord entity, HttpServletRequest request, Long id) {
        entity.setCreateId(id);
        entity.setCreateTime(new Date());
        entity.setUpdateId(id);
        entity.setUpdateTime(new Date());
        return chatRecordMapper.insert(entity);
    }

    @Override
    public IPage<ChatRecord> findAll(Page<ChatRecord> page, ChatRecordForm chatRecordForm) {
        return chatRecordMapper.findAll(page, chatRecordForm);
    }

    @Override
    public ChatRecord findByKey(Long id) {
        return chatRecordMapper.findByKey(id);
    }
}
