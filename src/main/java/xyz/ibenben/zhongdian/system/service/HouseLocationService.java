package xyz.ibenben.zhongdian.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.ibenben.zhongdian.system.entity.HouseLocation;

/**
 * 房屋位置服务类
 * 提供了一些基本的服务，如根据主键查找房屋位置
 * 是此系统的主要服务类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface HouseLocationService extends IService<HouseLocation> {
    /**
     * 根据房屋主键获取位置信息
     *
     * @param houseId 房屋主键
     * @return 位置实体类
     */
    HouseLocation selectByHouseId(Long houseId);
}
