package xyz.ibenben.zhongdian.system.service.sys.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import xyz.ibenben.zhongdian.common.annotation.SystemServiceLog;
import xyz.ibenben.zhongdian.common.constants.Constants;
import xyz.ibenben.zhongdian.system.entity.sys.SysRole;
import xyz.ibenben.zhongdian.system.form.sys.SysRoleForm;
import xyz.ibenben.zhongdian.system.mapper.sys.SysRoleMapper;
import xyz.ibenben.zhongdian.system.service.sys.SysRoleResourcesService;
import xyz.ibenben.zhongdian.system.service.sys.SysRoleService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 系统角色服务实现类
 * 系统级角色管理所使用的，是权限系统中的一部分
 * 提供了一些基本的服务，如查询角色列表、根据角色描述查找是否重复等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {
    @Resource
    private SysRoleMapper sysRoleMapper;
    @Resource
    private SysRoleResourcesService sysRoleResourcesService;

    /**
     * queryRoleListWithSelected角色记录
     *
     * @param uid 参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "queryRoleListWithSelected角色记录")
    public List<SysRole> queryRoleListWithSelected(Long uid) {
        return sysRoleMapper.queryRoleListWithSelected(uid);
    }

    /**
     * delete角色记录
     *
     * @param key     参数
     * @param request 参数
     * @return 返回值
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = {Exception.class})
    @SystemServiceLog(description = "delete角色记录")
    public int delete(Object key, HttpServletRequest request) {
        //删除角色资源
        int result = 0;
        int resultRR = sysRoleResourcesService.deleteByRoleId(key);
        //删除角色
        SysRole entity = this.getById((Serializable) key);
        entity.setDelFlag(0);
        entity.setDelId((Long) request.getSession().getAttribute(Constants.SESSIONID));
        entity.setDelTime(new Date());
        int resultR = sysRoleMapper.updateById(entity);
        if (resultRR != 0 && resultR != 0) {
            result = resultR;
        }
        return result;
    }

    /**
     * selectByRoleDesc角色记录
     *
     * @param companyId 公司主键
     * @param roledesc  参数
     * @return 返回值
     */
    @Override
    @SystemServiceLog(description = "selectByRoleDesc角色记录")
    public SysRole selectByRoleDesc(Long companyId, String roledesc) {
        SysRole role = null;
        QueryWrapper<SysRole> wrapper = new QueryWrapper<>();
        wrapper.eq("company_id", companyId);
        wrapper.eq("role_desc", roledesc);
        List<SysRole> roleList = sysRoleMapper.selectList(wrapper);
        if (!roleList.isEmpty()) {
            role = roleList.get(0);
        }
        return role;
    }

    @Override
    public SysRole selectRoleWithCompanyById(Long id) {
        return sysRoleMapper.selectRoleWithCompanyById(id);
    }

    @Override
    public IPage<SysRole> findAll(Page<SysRole> page, SysRoleForm sysRoleForm) {
        return sysRoleMapper.findAll(page, sysRoleForm);
    }
}
