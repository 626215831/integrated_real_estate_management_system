package xyz.ibenben.zhongdian.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import xyz.ibenben.zhongdian.system.entity.HouseStatus;
import xyz.ibenben.zhongdian.system.form.HouseStatusForm;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 房屋状态服务类
 * 提供了一些基本的服务，如根据房产证查找房屋状态记录列表等方法。
 * 属于产权下的一个分支
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface HouseStatusService extends IService<HouseStatus> {
    /**
     * 根据房产证查找房屋状态记录列表
     *
     * @param id 参数
     * @return 返回值
     */
    List<HouseStatus> findByOwnerId(Long id);

    /**
     * 根据拥有者主键删除记录
     *
     * @param id      拥有者主键
     * @param request 请求
     */
    void deleteByOwnerId(Long id, HttpServletRequest request);

    /**
     * 根据条件查询列表
     *
     * @param page            分页参数
     * @param houseStatusForm 条件
     * @return 列表
     */
    IPage<HouseStatus> findAll(Page<HouseStatus> page, HouseStatusForm houseStatusForm);
}
