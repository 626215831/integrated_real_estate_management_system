package xyz.ibenben.zhongdian.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import xyz.ibenben.zhongdian.system.entity.HouseInfo;
import xyz.ibenben.zhongdian.system.entity.ajax.AjaxJson;
import xyz.ibenben.zhongdian.system.form.HouseInfoForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 房屋信息服务类
 * 提供了一些基本的服务，如根据主键查找房屋信息及图片、
 * 根据房屋名称查询记录、导出列表等方法。
 * 是此系统的主要服务类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface HouseInfoService extends IService<HouseInfo> {
    /**
     * 根据主键查找房屋信息及图片
     *
     * @param id 参数
     * @return 返回值
     */
    HouseInfo findOneWithImage(Long id);

    /**
     * 根据房屋名称查询记录
     *
     * @param houseName 参数
     * @return 返回值
     */
    List<HouseInfo> findByHouseName(String houseName);

    /**
     * 导出列表
     *
     * @param result   参数
     * @param request  参数
     * @param response 参数
     */
    void export(List<Long> result, HttpServletRequest request,
                HttpServletResponse response);

    /**
     * 导入数据
     *
     * @param bytes 参数
     */
    void importData(byte[] bytes, HttpServletRequest request) throws Exception;

    /**
     * 根据用户主键和类型查找房屋信息
     *
     * @param id   参数
     * @param type 参数
     * @return 返回值
     */
    List<HouseInfo> findByCreateIdAndType(Long id, int type);

    /**
     * 获取房屋饼图信息
     *
     * @return 返回值
     */
    AjaxJson getHouseInfoList();

    /**
     * 根据主键获取位置信息及中文信息
     *
     * @param id 房屋主键
     * @return 带中文位置信息的实体类
     */
    HouseInfo findWithLocation(Long id);

    /**
     * 根据条件查找列表
     *
     * @param page          分页参数
     * @param houseInfoForm 条件
     * @return 列表
     */
    IPage<HouseInfo> findAll(Page<HouseInfo> page, HouseInfoForm houseInfoForm);

    /**
     * 获取房屋地图信息
     *
     * @return 房屋地图信息
     */
    Map<String, Object> getWholeChinaInfo();
}
