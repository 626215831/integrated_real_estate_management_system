package xyz.ibenben.zhongdian.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import xyz.ibenben.zhongdian.system.entity.InnerMail;
import xyz.ibenben.zhongdian.system.form.InnerMailForm;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 站内信服务类
 * 提供了一些基本的服务，如获取公告、查询个人消息、查询每条消息的详细信息等方法
 * 主要用于用户之间通信使用此类来维护
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface InnerMailService extends IService<InnerMail> {
    /**
     * 获取公告
     *
     * @param userId 参数
     * @param type   类型
     * @return 返回值
     */
    List<InnerMail> findAllNoticeByUserId(Long userId, Integer type);

    /**
     * 查询个人消息
     *
     * @param id      参数
     * @param request 参数
     * @return 返回值
     */
    InnerMail selectByKeyAndUserId(Long id, HttpServletRequest request);

    /**
     * 查询每条消息的详细信息
     *
     * @param id 参数
     * @return 返回值
     */
    InnerMail selectByKeyWithList(Long id);

    /**
     * 所有人的消息翻页查询
     *
     * @param title   参数
     * @param isRead  参数
     * @param request 参数
     * @return 返回值
     */
    List<InnerMail> getAllDetail(String title, Boolean isRead, HttpServletRequest request);

    /**
     * 获取未读消息数量
     *
     * @param userId 参数
     * @param type   类型
     * @return 返回值
     */
    int findCountByUserId(Long userId, int type);

    /**
     * 根据条件查询列表
     *
     * @param page          分页参数
     * @param innerMailForm 条件
     * @return 列表
     */
    IPage<InnerMail> findAll(Page<InnerMail> page, InnerMailForm innerMailForm);

}
