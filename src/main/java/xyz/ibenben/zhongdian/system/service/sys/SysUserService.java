package xyz.ibenben.zhongdian.system.service.sys;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import xyz.ibenben.zhongdian.system.entity.sys.SysUser;
import xyz.ibenben.zhongdian.system.form.sys.SysUserForm;

import java.util.List;

/**
 * 系统用户服务类
 * 系统级用户所使用的，是权限系统中的一部分
 * 提供了一些基本的服务，如根据用户名称查询用户信息列表、根据站内信类型获取用户等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public interface SysUserService extends IService<SysUser> {
    /**
     * 根据用户名称查询用户信息列表
     *
     * @param username 参数
     * @return 返回值
     */
    SysUser selectByUsername(String username);

    /**
     * 根据站内信类型获取用户
     *
     * @param ordinal 参数
     * @return 返回值
     */
    List<SysUser> findByType(int ordinal);

    /**
     * 查找给定字段是否存在
     *
     * @param email 参数
     * @return 返回值
     */
    SysUser checkExist(String email, String username);

    /**
     * 根据条件查找列表
     *
     * @param page        分页参数
     * @param sysUserForm 条件
     * @return 列表
     */
    IPage<SysUser> findAll(Page<SysUser> page, SysUserForm sysUserForm);

    /**
     * 根据主键查找记录
     *
     * @param id 主键
     * @return 记录
     */
    SysUser findByKey(Long id);

    /**
     * 根据用户名称获取用户信息
     *
     * @param username 用户名称
     * @return 用户信息
     */
    SysUser selectUserWithRoleByUsername(String username);

}
