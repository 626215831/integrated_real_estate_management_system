package xyz.ibenben.zhongdian.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.ibenben.zhongdian.common.annotation.SystemServiceLog;
import xyz.ibenben.zhongdian.system.entity.HouseImage;
import xyz.ibenben.zhongdian.system.entity.HouseInfo;
import xyz.ibenben.zhongdian.system.entity.RentingHouse;
import xyz.ibenben.zhongdian.system.entity.enums.HouseTypeEnum;
import xyz.ibenben.zhongdian.system.form.RentingHouseForm;
import xyz.ibenben.zhongdian.system.mapper.HouseImageMapper;
import xyz.ibenben.zhongdian.system.mapper.RentingHouseMapper;
import xyz.ibenben.zhongdian.system.service.HouseInfoService;
import xyz.ibenben.zhongdian.system.service.RentingHouseService;

import javax.annotation.Resource;
import java.util.List;

/**
 * 租赁房屋服务实现类
 * 提供了一些基本的服务，如获取图像等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Service
public class RentingHouseServiceImpl extends ServiceImpl<RentingHouseMapper, RentingHouse> implements RentingHouseService {
    @Resource
    private RentingHouseMapper rentingHouseMapper;
    @Resource
    private HouseInfoService houseInfoService;
    @Resource
    private HouseImageMapper houseImageMapper;

    /**
     * save租房信息
     *
     * @param entity 租房的实体
     * @return 是否成功的整形值
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    @SystemServiceLog(description = "save租房信息")
    public boolean save(RentingHouse entity) {
        //获取房屋信息并更新
        HouseInfo info = houseInfoService.getById(entity.getHouseId());
        info.setType(HouseTypeEnum.RENTING);
        houseInfoService.updateById(info);
        return this.save(entity);
    }

    /**
     * findOneWithImage租房信息
     *
     * @param id 房屋主键
     * @return 租赁房屋实体
     */
    @Override
    @SystemServiceLog(description = "findOneWithImage租房信息")
    public RentingHouse findOneWithImage(Long id) {
        RentingHouse house = rentingHouseMapper.selectById(id);
        HouseInfo info = houseInfoService.findOneWithImage(house.getHouseId());
        house.setInfo(info);
        //获取房屋图像信息
        QueryWrapper<HouseImage> wrapper = new QueryWrapper<>();
        wrapper.eq("house_id", id);
        List<HouseImage> list = houseImageMapper.selectList(wrapper);
        if (list != null && !list.isEmpty()) {
            house.setList(list);
        }
        return house;
    }

    /**
     * 根据标题获取记录
     *
     * @param title 标题
     * @return 租赁信息
     */
    @Override
    public List<RentingHouse> findByTitle(String title) {
        QueryWrapper<RentingHouse> wrapper = new QueryWrapper<>();
        wrapper.eq("title", title);
        return rentingHouseMapper.selectList(wrapper);
    }

    @Override
    public IPage<RentingHouse> findAll(Page<RentingHouse> page, RentingHouseForm rentingHouseForm) {
        return rentingHouseMapper.findAll(page, rentingHouseForm);
    }
}
