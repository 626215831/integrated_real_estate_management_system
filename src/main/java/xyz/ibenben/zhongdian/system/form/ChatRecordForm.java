package xyz.ibenben.zhongdian.system.form;

import lombok.Getter;
import lombok.Setter;
import xyz.ibenben.zhongdian.system.entity.enums.ChatTypeEnum;

/**
 * 聊天记录查询条件类
 * 提供了对内容,聊天类型等字段的查询。
 * 并使用Example把所得到的结果整合为实例，最后通过服务实现类调用数据库查询结果
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
public class ChatRecordForm extends SearchForm {
    private static final long serialVersionUID = -2465644184106576265L;

    /**
     * 内容
     */
    protected String content;

    /**
     * 聊天类型 1 聊天室 2私聊
     */
    protected ChatTypeEnum type;

    /**
     * 发送者
     */
    protected Long fromId;

    /**
     * 接收者
     */
    protected Long toId;

}
