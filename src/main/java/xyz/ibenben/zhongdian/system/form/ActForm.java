package xyz.ibenben.zhongdian.system.form;

import lombok.Getter;
import lombok.Setter;

/**
 * 流程查询条件类
 * 提供了对内容,聊天类型等字段的查询。
 * 并使用Example把所得到的结果整合为实例，最后通过服务实现类调用数据库查询结果
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
public class ActForm extends SearchForm {
    private static final long serialVersionUID = -2465644184106576265L;

    /**
     * 标题
     */
    protected String title;

    /**
     * key
     */
    protected String key;

}
