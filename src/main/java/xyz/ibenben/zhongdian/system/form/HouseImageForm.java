package xyz.ibenben.zhongdian.system.form;

import lombok.Getter;
import lombok.Setter;
import xyz.ibenben.zhongdian.system.entity.enums.ImageTypeEnum;

/**
 * 房屋图像查询条件类
 * 提供了对图像类型等字段的查询。
 * 并使用Example把所得到的结果整合为实例，最后通过服务实现类调用数据库查询结果
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
public class HouseImageForm extends SearchForm {
    private static final long serialVersionUID = -5940792477523924065L;

    /**
     * 图像类型
     */
    protected ImageTypeEnum type;

}
