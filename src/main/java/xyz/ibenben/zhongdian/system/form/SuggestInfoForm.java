package xyz.ibenben.zhongdian.system.form;

import lombok.Getter;
import lombok.Setter;
import xyz.ibenben.zhongdian.system.entity.enums.SuggestTypeEnum;

/**
 * 意见及建议查询条件类
 * 提供了对类型,建议或意见等字段的查询。
 * 并使用Example把所得到的结果整合为实例，最后通过服务实现类调用数据库查询结果
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
public class SuggestInfoForm extends SearchForm {
    private static final long serialVersionUID = -2465644184106576265L;

    /**
     * 用户主键
     */
    private Long userId;

    /**
     * 评分
     */
    private Integer goal;

    /**
     * 联系方式
     */
    private String phone;

    /**
     * 电子邮箱
     */
    private String email;

    /**
     * 建议或意见
     */
    private String content;

    /**
     * 类型 0-意见 1-建议
     */
    private SuggestTypeEnum type;

}
