package xyz.ibenben.zhongdian.system.form;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.enums.StuffStatusEnum;

/**
 * 物品名称信息查询条件类
 * 提供了对房屋物品名称，物品名称数量等字段的查询。
 * 并使用Example把所得到的结果整合为实例，最后通过服务实现类调用数据库查询结果
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
@ToString
public class StuffInfoForm extends SearchForm {
    private static final long serialVersionUID = 6121524867379737233L;

    /**
     * 物品名称
     */
    protected String name;

    /**
     * 物品状态
     */
    protected StuffStatusEnum status;

    /**
     * 租赁主键
     */
    protected Long rentingId;

}
