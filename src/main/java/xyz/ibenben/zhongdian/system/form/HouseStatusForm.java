package xyz.ibenben.zhongdian.system.form;

import lombok.Getter;
import lombok.Setter;
import xyz.ibenben.zhongdian.system.entity.enums.HouseStructureEnum;
import xyz.ibenben.zhongdian.system.entity.enums.StatusTypeEnum;

import java.math.BigDecimal;

/**
 * 房屋状态查询条件类
 * 提供了对拥有者，所在楼层类型等字段的查询。
 * 并使用Example把所得到的结果整合为实例，最后通过服务实现类调用数据库查询结果
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
public class HouseStatusForm extends SearchForm {
    private static final long serialVersionUID = 241327111022525056L;

    /**
     * 拥有者
     */
    protected Long ownerId;

    /**
     * 所在楼层
     */
    protected Integer houseInLayer;

    /**
     * 建筑面积
     */
    protected BigDecimal coveredArea;

    /**
     * 使用面积
     */
    protected BigDecimal useableArea;

    /**
     * 键
     */
    protected String key;

    /**
     * 房屋结构
     */
    protected HouseStructureEnum structure;

    /**
     * 状态类型
     */
    protected StatusTypeEnum type;

}
