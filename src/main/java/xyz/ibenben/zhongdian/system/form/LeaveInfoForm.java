package xyz.ibenben.zhongdian.system.form;

import lombok.Getter;
import lombok.Setter;
import xyz.ibenben.zhongdian.system.entity.enums.LeaveTypeEnum;

/**
 * 请假申请查询条件类
 * 提供了对名称,请假类型等字段的查询。
 * 并使用Example把所得到的结果整合为实例，最后通过服务实现类调用数据库查询结果
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
public class LeaveInfoForm extends SearchForm {
    private static final long serialVersionUID = -2465644184106576265L;

    /**
     * 名称
     */
    protected String name;

    /**
     * 请假类型
     */
    protected LeaveTypeEnum type;

    /**
     * 原因
     */
    protected String reason;

}
