package xyz.ibenben.zhongdian.system.form;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.enums.DealRecordTypeEnum;

/**
 * 成交记录信息查询条件类
 * 提供了对成交记录名称，成交记录类型等字段的查询。
 * 并使用Example把所得到的结果整合为实例，最后通过服务实现类调用数据库查询结果
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
@ToString
public class DealRecordInfoForm extends SearchForm {
    private static final long serialVersionUID = 6121524867379737233L;

    /**
     * 购买人
     */
    protected String buyer;

    /**
     * 联系电话
     */
    protected String phone;

    /**
     * 成交类型
     */
    protected DealRecordTypeEnum dealType;

    /**
     * 省
     */
    protected Long province;

    /**
     * 市
     */
    protected Long city;

    /**
     * 区
     */
    protected Long region;

}
