package xyz.ibenben.zhongdian.system.form.sys;

import lombok.Getter;
import lombok.Setter;
import xyz.ibenben.zhongdian.system.entity.enums.ResourceTypeEnum;
import xyz.ibenben.zhongdian.system.form.SearchForm;

/**
 * 系统资源查询条件类
 * 提供了对资源名称,资源类型等字段的查询。
 * 并使用Example把所得到的结果整合为实例，最后通过服务实现类调用数据库查询结果
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
public class SysResourcesForm extends SearchForm {
    private static final long serialVersionUID = 7065149514749025010L;

    /**
     * 资源名称
     */
    protected String name;

    /**
     * 资源类型
     */
    protected ResourceTypeEnum type;

}
