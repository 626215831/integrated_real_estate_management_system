package xyz.ibenben.zhongdian.system.entity.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * 经验状态枚举类
 * 包括开发商，中介等
 * 使用枚举类可以有效的节省代码行数，调整系统的耦合度
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
public enum CompanyStatusEnum {
    SUBSIST(0, "存续"), EMPLOYED(1, "在业"), REVOKE(2, "吊销"), CANCELLATION(3, "注销"), INTO(4, "迁入"), OUTTO(5, "迁出"),
    SHUTDOWN(6, "停业"), LIQUIDATION(7, "清算");

    /* Map对象 */
    private static final Map<Integer, CompanyStatusEnum> valueMap = new HashMap<>();

    static {
        for (CompanyStatusEnum mail : CompanyStatusEnum.values()) {
            valueMap.put(mail.getValue(), mail);
        }
    }

    /* 值 */
    @EnumValue
    private final int value;
    /* 文字 */
    private final String text;

    /**
     * 构造方法
     *
     * @param value
     * @param text
     */
    CompanyStatusEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * 获取值
     *
     * @param value 参数
     * @return 返回值
     */
    public static CompanyStatusEnum getByValue(int value) {
        CompanyStatusEnum result = valueMap.get(value);
        if (result == null) {
            throw new IllegalArgumentException("No element matches " + value);
        }
        return result;
    }

    /**
     * 获取文字
     *
     * @param text 参数
     * @return 返回值
     */
    public static CompanyStatusEnum getByText(String text) {
        for (CompanyStatusEnum e : values()) {
            if (e.getText().equals(text)) {
                return e;
            }
        }
        return null;
    }

}