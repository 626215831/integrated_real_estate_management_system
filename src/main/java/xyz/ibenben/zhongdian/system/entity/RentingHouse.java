package xyz.ibenben.zhongdian.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.enums.PayTypeEnum;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 房屋租赁实体类
 * 记录该表记录了房屋主键，房屋所有人等字段
 * 表名是renting_house
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
@ToString
public class RentingHouse extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 4790488995597270526L;

    /**
     * 房屋主键
     */
    private Long houseId;

    /**
     * 房屋所有人
     */
    private String owner;

    /**
     * 出租价格
     */
    private BigDecimal price;

    /**
     * 支付类型
     */
    private PayTypeEnum priceType;

    /**
     * 租金包含费用
     */
    private String includeService;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 装修风格
     */
    private String fitment;

    /**
     * 提供服务
     */
    private String supply;

    /**
     * 标题
     */
    private String title;

    /**
     * 房源描述
     */
    private String description;

    /**
     * 房屋信息
     */
    @TableField(exist = false)
    private HouseInfo info;

    /**
     * 房屋图片
     */
    @TableField(exist = false)
    private List<HouseImage> list;

}
