package xyz.ibenben.zhongdian.system.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 日志实体类
 * 记录该表记录了描述，方法等字段
 * 表名是log
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
@Setter
@ToString
public class Log extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 4254130484943935732L;

    /**
     * 描述
     */
    private String description;

    /**
     * 方法
     */
    private String method;

    /**
     * 类型
     */
    private String type;

    /**
     * 请求IP地址
     */
    private String requestIp;

    /**
     * 异常代码
     */
    private String exceptionCode;

    /**
     * 异常信息
     */
    private String exceptionDetail;

    /**
     * 参数
     */
    private String params;

}
