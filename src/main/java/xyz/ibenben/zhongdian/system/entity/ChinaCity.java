package xyz.ibenben.zhongdian.system.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 城市实体类
 * 记录该表记录了省份代码，城市代码等字段
 * 表名是china_city
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
@Setter
@ToString
public class ChinaCity extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 7385041158809959066L;

    /**
     * 省份代码
     */
    private Long provinceCode;

    /**
     * 城市代码
     */
    private Long cityCode;

    /**
     * 城市名称
     */
    private String cityName;

}
