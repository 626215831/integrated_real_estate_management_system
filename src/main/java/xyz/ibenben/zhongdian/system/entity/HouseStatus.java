package xyz.ibenben.zhongdian.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.enums.HouseStructureEnum;
import xyz.ibenben.zhongdian.system.entity.enums.StatusTypeEnum;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 房屋状态实体类
 * 记录该表记录了拥有者，厅数量等字段
 * 表名是house_status
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
@Setter
@ToString
public class HouseStatus extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1786551986601811472L;

    /**
     * 拥有者
     */
    private Long ownerId;

    /**
     * 厅数量
     */
    private String hitNumber;

    /**
     * 室数量
     */
    private String roomNumber;

    /**
     * 房屋结构
     */
    private HouseStructureEnum structure;

    /**
     * 总楼层
     */
    private Integer houseTotalLayer;

    /**
     * 所在楼层
     */
    private Integer houseInLayer;

    /**
     * 建筑面积
     */
    private BigDecimal coveredArea;

    /**
     * 使用面积
     */
    private BigDecimal useableArea;

    /**
     * 设计目的
     */
    private String designPurpose;

    /**
     * 状态类型
     */
    private StatusTypeEnum type;

    /**
     * 从拥有者主键
     */
    @TableField(exist = false)
    private boolean fromOwnerId = false;

}
