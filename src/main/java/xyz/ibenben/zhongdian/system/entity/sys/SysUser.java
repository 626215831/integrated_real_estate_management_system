package xyz.ibenben.zhongdian.system.entity.sys;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.BaseEntity;

import java.io.Serializable;

/**
 * 系统用户实体类
 * 记录该表记录了用户名称，密码等字段
 * 表名是sys_user
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
@ToString
public class SysUser extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -8736616045315083846L;

    /**
     * 用户名称
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 确认密码
     */
    @TableField(exist = false)
    private String cpassword;

    /**
     * 新密码
     */
    @TableField(exist = false)
    private String npassword;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 地址
     */
    private String address;

    /**
     * 所在省代码
     */
    private Long province;

    /**
     * 所在市代码
     */
    private Long city;

    /**
     * 所在区代码
     */
    private Long region;

    /**
     * 前景色
     */
    private String fontColor;

    /**
     * 背景色
     */
    private String backColor;

    /**
     * 记住我
     */
    @TableField(exist = false)
    private String rememberMe;

    /**
     * 是否启用
     */
    private Integer enable;

    /**
     * 类型
     */
    private Boolean type;

    /**
     * 头像
     */
    private String head;

    /**
     * 图像
     */
    @TableField(exist = false)
    private byte[] image;

    /**
     * 用户角色信息
     */
    @TableField(exist = false)
    private SysRole sysRole;

    /**
     * 资源地址
     */
    @TableField(exist = false)
    private String resUrl;
}