package xyz.ibenben.zhongdian.system.entity.ajax;

import net.sf.json.JSONObject;
import xyz.ibenben.zhongdian.common.constants.Constants;

import java.util.Map;

/**
 * $.ajax后需要接受的JSON
 * 通过Json对前端rest接口来实现的统一返回规则类
 * 经过不断调整来适应所有接口的规则
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
public class AjaxJson {
    private final JSONObject jsonObject = new JSONObject();

    /**
     * 获取Json对象
     *
     * @return 返回值
     */
    public JSONObject getJsonObject() {
        if (jsonObject.get(Constants.SUCCESS) == null) {
            jsonObject.put(Constants.SUCCESS, true);
        }
        return jsonObject;
    }

    /* 以下为AjaxJson可容纳类型 */

    /**
     * 设置属性
     *
     * @param attributes 参数
     */
    public void setAttributes(Map<String, Object> attributes) {
        jsonObject.put("attributes", attributes);
    }

    /**
     * 设置信息
     *
     * @param msg 参数
     */
    public void setMsg(String msg) {
        jsonObject.put(Constants.MSG, msg);
    }

    /**
     * 设置对象
     *
     * @param obj 参数
     */
    public void setObj(Object obj) {
        jsonObject.put("obj", obj);
    }

    /**
     * 设置是否成功
     *
     * @param success
     */
    public void setSuccess(boolean success) {
        jsonObject.put(Constants.SUCCESS, success);
    }

    /* 以上为AjaxJson可容纳类型 */
}
