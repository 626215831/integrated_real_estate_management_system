package xyz.ibenben.zhongdian.system.entity.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * 请假类型枚举类
 * 包括事假，病假等
 * 使用枚举类可以有效的节省代码行数，调整系统的耦合度
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
public enum LeaveTypeEnum {
    /**
     * 请假类型
     */
    THING(1, "事假"), ILL(2, "病假"),
    CHANGE(3, "调休"), MARRY(4, "婚假"),
    DEAD(5, "丧假"), OTHER(6, "其他");

    /* Map对象 */
    private static final Map<Integer, LeaveTypeEnum> valueMap = new HashMap<>();

    static {
        for (LeaveTypeEnum mail : LeaveTypeEnum.values()) {
            valueMap.put(mail.getValue(), mail);
        }
    }

    /* 值 */
    @EnumValue
    private final int value;
    /* 文字 */
    private final String text;

    /**
     * 构造方法
     *
     * @param value
     * @param text
     */
    LeaveTypeEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * 获取值
     *
     * @param value 参数
     * @return 返回值
     */
    public static LeaveTypeEnum getByValue(int value) {
        LeaveTypeEnum result = valueMap.get(value);
        if (result == null) {
            throw new IllegalArgumentException("No element matches " + value);
        }
        return result;
    }

    /**
     * 获取文字
     *
     * @param text 参数
     * @return 返回值
     */
    public static LeaveTypeEnum getByText(String text) {
        for (LeaveTypeEnum e : values()) {
            if (e.getText().equals(text)) {
                return e;
            }
        }
        return null;
    }

}