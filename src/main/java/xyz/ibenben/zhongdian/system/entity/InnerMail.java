package xyz.ibenben.zhongdian.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.enums.MailTypeEnum;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 站内信实体类
 * 记录该表记录了标题，内容等字段
 * 表名是inner_mail
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
@Setter
@ToString
public class InnerMail extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -1847703885928213412L;

    /**
     * 标题
     */
    private String title;

    /**
     * 内容
     */
    private String content;

    /**
     * 站内信类型 1后台公告 2前台公告 3后台私信 4前台私信
     */
    private MailTypeEnum type;

    /**
     * 发送者
     */
    private Long fromId;

    /**
     * 接收者
     */
    @TableField(exist = false)
    private Long toId;

    /**
     * 发送者名称
     */
    @TableField(exist = false)
    private String fromUserName;

    /**
     * 接收者名称
     */
    @TableField(exist = false)
    private String toUserName;

    /**
     * 是否已读 0未读 1已读
     */
    @TableField(exist = false)
    private Boolean isRead;

    /**
     * 发送时间
     */
    @TableField(exist = false)
    private Date sendTime;

    /**
     * 发送内容
     */
    @TableField(exist = false)
    private List<InnerMailRecord> list;

}
