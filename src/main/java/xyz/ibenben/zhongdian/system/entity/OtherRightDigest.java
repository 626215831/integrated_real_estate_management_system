package xyz.ibenben.zhongdian.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.enums.RightTypeEnum;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 他项权利摘要实体类
 * 记录该表记录了拥有者，权利人等字段
 * 表名是other_right_digest
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
@ToString
public class OtherRightDigest extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 8416303275004427185L;

    /**
     * 拥有者
     */
    private Long ownerId;

    /**
     * 权利人
     */
    private String righter;

    /**
     * 权利种类
     */
    private RightTypeEnum type;

    /**
     * 权利范围
     */
    private String ranges;

    /**
     * 权利价值
     */
    private BigDecimal price;

    /**
     * 设定日期
     */
    private Date setDate;

    /**
     * 约定期限
     */
    private Date dateLimit;

    /**
     * 注销日期
     */
    private Date logoutDate;

    /**
     * 从拥有者主键
     */
    @TableField(exist = false)
    private boolean fromOwnerId = false;

}
