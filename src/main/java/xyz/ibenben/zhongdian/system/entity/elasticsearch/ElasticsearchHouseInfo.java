package xyz.ibenben.zhongdian.system.entity.elasticsearch;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import xyz.ibenben.zhongdian.system.entity.HouseImage;
import xyz.ibenben.zhongdian.system.entity.HouseLocation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 房屋信息实体类
 * 记录该表记录了房屋名称，室数量路径等字段
 * 表名是house_info
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
@Setter
@ToString
@Document(indexName = "zhongdian", type = "houseInfo")
public class ElasticsearchHouseInfo extends BaseEntityForEs implements Serializable {
    private static final long serialVersionUID = 5441997212327447070L;

    /**
     * 房屋名称
     */
    @Field(type = FieldType.Keyword)
    private String houseName;

    /**
     * 室数量
     */
    @Field(type = FieldType.Keyword)
    private Integer roomNumber = 0;

    /**
     * 厅数量
     */
    @Field(type = FieldType.Keyword)
    private Integer drawingNumber = 0;

    /**
     * 厨数量
     */
    @Field(type = FieldType.Keyword)
    private Integer kitchenNumber = 0;

    /**
     * 卫数量
     */
    @Field(type = FieldType.Keyword)
    private Integer restroomNumber = 0;

    /**
     * 朝向
     */
    @Field
    private String orientationName;

    /**
     * 房屋面积
     */
    @Field(type = FieldType.Keyword)
    private BigDecimal houseArea;

    /**
     * 所在楼层
     */
    @Field(type = FieldType.Keyword)
    private Integer layer;

    /**
     * 总楼层
     */
    @Field(type = FieldType.Keyword)
    private Integer totalLayer;

    /**
     * 小区
     */
    @Field(type = FieldType.Text)
    private String polt;

    /**
     * 详细地址
     */
    @Field(type = FieldType.Text)
    private String address;

    /**
     * 住宅用途
     */
    @Field(type = FieldType.Text)
    private String purpose;

    /**
     * 购买时间
     */
    @Field(type = FieldType.Date)
    private Date buyTime;

    /**
     * 产权
     */
    @Field(type = FieldType.Keyword)
    private Integer propertyRight;

    /**
     * 房屋类型
     */
    @Field
    private String typeName;

    /**
     * 房屋图像
     */
    @Field
    private List<HouseImage> list;

    /**
     * 房屋经纬度
     */
    @Field
    private HouseLocation houseLocation;
}
