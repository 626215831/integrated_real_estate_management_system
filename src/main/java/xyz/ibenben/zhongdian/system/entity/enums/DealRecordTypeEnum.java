package xyz.ibenben.zhongdian.system.entity.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * 成交记录类型枚举类
 * 包括预售中，租赁中等
 * 使用枚举类可以有效的节省代码行数，调整系统的耦合度
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
public enum DealRecordTypeEnum {
    BOOKING(1, "预售"), RENTING(2, "租赁"), BUY(3, "购买"), OTHER(4, "其他");

    /* Map对象 */
    private static final Map<Integer, DealRecordTypeEnum> valueMap = new HashMap<>();

    static {
        for (DealRecordTypeEnum mail : DealRecordTypeEnum.values()) {
            valueMap.put(mail.getValue(), mail);
        }
    }

    /* 值 */
    @EnumValue
    private final int value;
    /* 文字 */
    private final String text;

    /**
     * 构造方法
     *
     * @param value
     * @param text
     */
    DealRecordTypeEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * 获取值
     *
     * @param value 参数
     * @return 返回值
     */
    public static DealRecordTypeEnum getByValue(int value) {
        DealRecordTypeEnum result = valueMap.get(value);
        if (result == null) {
            throw new IllegalArgumentException("No element matches " + value);
        }
        return result;
    }

    /**
     * 获取文字
     *
     * @param text 参数
     * @return 返回值
     */
    public static DealRecordTypeEnum getByText(String text) {
        for (DealRecordTypeEnum e : values()) {
            if (e.getText().equals(text)) {
                return e;
            }
        }
        return null;
    }

}