package xyz.ibenben.zhongdian.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.enums.ChatTypeEnum;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Date;

/**
 * 聊天记录实体类
 * 记录该表记录了发送者，接收者等聊天消息
 * 表名是chat_record
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
@Setter
@ToString
public class ChatRecord extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -1847703885928213412L;

    /**
     * 发送者
     */
    private Long fromId;

    /**
     * 接收者
     */
    private Long toId;

    /**
     * 内容
     */
    @NotEmpty(message = "内容不能为空")
    private String content;

    /**
     * 发送时间
     */
    private Date sendTime;

    /**
     * 发送类型 1 聊天室 2私聊
     */
    private ChatTypeEnum type;

    /**
     * 前景色
     */
    @TableField(exist = false)
    private String fontColor;

    /**
     * 背景色
     */
    @TableField(exist = false)
    private String backColor;

    /**
     * 发送用户名
     */
    @TableField(exist = false)
    private String fromUserName;

    /**
     * 标题
     */
    @TableField(exist = false)
    private String head;

    /**
     * 接收用户名
     */
    @TableField(exist = false)
    private String toUserName;

}
