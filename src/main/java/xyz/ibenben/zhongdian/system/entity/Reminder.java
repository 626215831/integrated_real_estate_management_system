package xyz.ibenben.zhongdian.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.sys.SysUser;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统提醒实体类
 * 记录该表记录了需要提醒的时间，需要提醒的内容等字段
 * 表名是reminder
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
@Setter
@ToString
public class Reminder extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 4254130484943935732L;

    /**
     * 用户主键
     */
    private Long userId;

    /**
     * 用户信息
     */
    @TableField(exist = false)
    private SysUser sysUser;

    /**
     * 需要提醒的内容
     */
    private String title;

    /**
     * 开始时间
     */
    private Date start;

    /**
     * 结束时间
     */
    private Date end;

    /**
     * 是否为全日事件
     */
    private Boolean allDay;

    /**
     * 是否需要提醒
     */
    private Boolean remind;

    /**
     * 需要提醒的时间
     */
    private Date remindTime;

    /**
     * 字体颜色
     */
    private String textColor;

    /**
     * 背景颜色
     */
    private String color;

}
