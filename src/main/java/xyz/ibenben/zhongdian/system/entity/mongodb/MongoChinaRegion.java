package xyz.ibenben.zhongdian.system.entity.mongodb;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;

/**
 * 区县实体类
 * 记录该表记录了省份代码，城市代码等字段
 * 表名是china_region
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
@Setter
@ToString
public class MongoChinaRegion extends BaseEntityForMongo implements Serializable {
    private static final long serialVersionUID = 1080389537340314318L;

    /**
     * 省份代码
     */
    @Field
    private Long provinceCode;

    /**
     * 城市代码
     */
    @Field
    private Long cityCode;

    /**
     * 区县代码
     */
    @Field
    private Long regionCode;

    /**
     * 区县名称
     */
    @Field
    private String regionName;

}
