package xyz.ibenben.zhongdian.system.entity.sys;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.BaseEntity;
import xyz.ibenben.zhongdian.system.entity.enums.ResourceTypeEnum;

import java.io.Serializable;

/**
 * 系统资源实体类
 * 记录该表记录了资源名称，资源url等字段
 * 表名是sys_resources
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
@ToString
public class SysResources extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -6812242071705361506L;

    /**
     * 资源名称
     */
    private String name;

    /**
     * 资源url
     */
    private String resUrl;

    /**
     * 资源类型   1:菜单	2：按钮
     */
    private ResourceTypeEnum type;

    /**
     * 父资源
     */
    private Long parentId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 是否选中
     */
    @TableField(exist = false)
    private String checked;

    /**
     * 是否leaf
     */
    @TableField(exist = false)
    private Boolean isLeaf = false;

}