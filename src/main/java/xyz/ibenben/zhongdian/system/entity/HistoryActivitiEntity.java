package xyz.ibenben.zhongdian.system.entity;

import lombok.Data;

import java.util.Date;

@Data
public class HistoryActivitiEntity {
    /**
     * id
     */
    private String id;
    /**
     * 名称
     */
    private String name;
    /**
     * 申请时间
     */
    private Date startTime;
    /**
     * 状态
     */
    private String status;
    /**
     * 描述
     */
    private String description;
    /**
     * 执行ID
     */
    private String executionId;
}
