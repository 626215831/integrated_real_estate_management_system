package xyz.ibenben.zhongdian.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 房屋预售实体类
 * 记录该表记录了房屋主键，房屋名称等字段
 * 表名是house_booking
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
@Setter
@ToString
public class HouseBooking extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 8875856021674957449L;

    /**
     * 房屋主键
     */
    private Long houseId;

    /**
     * 房屋名称
     */
    private String name;

    /**
     * 房屋地址
     */
    private String address;

    /**
     * 所在省代码
     */
    private Long province;

    /**
     * 所在市代码
     */
    private Long city;

    /**
     * 所在区代码
     */
    private Long region;

    /**
     * 开盘价格
     */
    private BigDecimal openPrice;

    /**
     * 建筑面积
     */
    private BigDecimal houseArea;

    /**
     * 售楼电话
     */
    private String phone;

    /**
     * 开发商
     */
    private String developer;

    /**
     * 登记号码
     */
    private String loginNumber;

    /**
     * 备注
     */
    private String memo;

    /**
     * 房屋信息
     */
    @TableField(exist = false)
    private HouseInfo info = new HouseInfo();

    /**
     * 房屋图像
     */
    @TableField(exist = false)
    private List<HouseImage> list;
}
