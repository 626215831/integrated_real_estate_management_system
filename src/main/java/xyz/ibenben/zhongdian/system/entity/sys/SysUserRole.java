package xyz.ibenben.zhongdian.system.entity.sys;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 系统用户角色实体类（多对多关系表）
 * 记录该表记录了角色主键，用户主键等字段
 * 表名是sys_user_role
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
@Setter
@ToString
public class SysUserRole implements Serializable {
    private static final long serialVersionUID = -916411139749530670L;

    /**
     * 用户主键
     */
    private Long userId;

    /**
     * 角色主键
     */
    private Long roleId;

    @TableField(exist = false)
    private String roleIds;
}