package xyz.ibenben.zhongdian.system.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 房产证实体类
 * 记录该表记录了房屋所有权人，房屋坐落等字段
 * 表名是property_ownership_certificate
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Setter
@Getter
@ToString
public class PropertyOwnershipCertificate extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 3761049036469888143L;

    /**
     * 房屋所有权人
     */
    private String ownershiper;

    /**
     * 房屋坐落
     */
    private String address;

    /**
     * 房屋性质
     */
    private String property;

    /**
     * 规划用途
     */
    private String purpose;

    /**
     * 共有情况
     */
    private String share;

    /**
     * 建筑面积
     */
    private BigDecimal coveredArea;

    /**
     * 使用面积
     */
    private BigDecimal useableArea;

    /**
     * 发证单位
     */
    private String certificationUnit;

    /**
     * 开始使用年限
     */
    private Date yearLimitStart;

    /**
     * 结束使用年限
     */
    private Date yearLimitEnd;

    /**
     * 登记日期
     */
    private Date getCardDate;

    /**
     * 登记字号
     */
    private String loginNumber;

    /**
     * 地号
     */
    private String landId;

    /**
     * 土地使用权取得方式
     */
    private String landMethod;

}
