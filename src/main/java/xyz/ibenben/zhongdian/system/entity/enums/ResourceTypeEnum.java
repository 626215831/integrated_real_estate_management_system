package xyz.ibenben.zhongdian.system.entity.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * 资源类型枚举类
 * 包括根，菜单等
 * 使用枚举类可以有效的节省代码行数，调整系统的耦合度
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
public enum ResourceTypeEnum {
    MAIN(0, "根"), MENU(1, "菜单"), BITTON(2, "按钮"), COLIMN(3, "行权限"), ROW(4, "列权限");

    /* Map对象 */
    private static final Map<Integer, ResourceTypeEnum> valueMap = new HashMap<>();

    static {
        for (ResourceTypeEnum mail : ResourceTypeEnum.values()) {
            valueMap.put(mail.getValue(), mail);
        }
    }

    /* 值 */
    @EnumValue
    private final int value;
    /* 文字 */
    private final String text;

    /**
     * 构造方法
     *
     * @param value
     * @param text
     */
    ResourceTypeEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * 获取值
     *
     * @param value 参数
     * @return 返回值
     */
    public static ResourceTypeEnum getByValue(int value) {
        ResourceTypeEnum result = valueMap.get(value);
        if (result == null) {
            throw new IllegalArgumentException("No element matches " + value);
        }
        return result;
    }

    /**
     * 获取文字
     *
     * @param text 参数
     * @return 返回值
     */
    public static ResourceTypeEnum getByText(String text) {
        for (ResourceTypeEnum e : values()) {
            if (e.getText().equals(text)) {
                return e;
            }
        }
        return null;
    }

}