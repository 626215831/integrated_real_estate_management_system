package xyz.ibenben.zhongdian.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.enums.HouseTypeEnum;
import xyz.ibenben.zhongdian.system.entity.enums.OrientationEnum;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 房屋信息实体类
 * 记录该表记录了房屋名称，室数量路径等字段
 * 表名是house_info
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
@Setter
@ToString
public class HouseInfo extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 5441997212327447070L;

    /**
     * 公司主键
     */
    private Long companyId;

    /**
     * 公司信息
     */
    @TableField(exist = false)
    private CompanyInfo companyInfo;

    /**
     * 房屋名称
     */
    @NotEmpty(message = "房屋名称不能为空")
    private String houseName;

    /**
     * 室数量
     */
    @NotNull(message = "室数量不能为空")
    private Integer roomNumber = 0;

    /**
     * 厅数量
     */
    @NotNull(message = "厅数量不能为空")
    private Integer drawingNumber = 0;

    /**
     * 厨数量
     */
    @NotNull(message = "厨数量不能为空")
    private Integer kitchenNumber = 0;

    /**
     * 卫数量
     */
    @NotNull(message = "卫数量不能为空")
    private Integer restroomNumber = 0;

    /**
     * 朝向
     */
    @NotNull(message = "朝向不能为空")
    private OrientationEnum orientation;

    /**
     * 房屋面积
     */
    @NotNull(message = "房屋面积不能为空")
    private BigDecimal houseArea;

    /**
     * 所在楼层
     */
    @NotNull(message = "所在楼层不能为空")
    private Integer layer;

    /**
     * 总楼层
     */
    @NotNull(message = "总楼层不能为空")
    private Integer totalLayer;

    /**
     * 小区
     */
    private String polt;

    /**
     * 详细地址
     */
    @NotEmpty(message = "详细地址不能为空")
    private String address;

    /**
     * 所在省
     */
    @NotNull(message = "所在省不能为空")
    private String province;

    /**
     * 所在市
     */
    @NotNull(message = "所在市不能为空")
    private String city;

    /**
     * 所在区
     */
    @NotNull(message = "所在区不能为空")
    private String region;

    /**
     * 住宅用途
     */
    @NotEmpty(message = "住宅用途不能为空")
    private String purpose;

    /**
     * 购买时间
     */
    @NotNull(message = "购买时间不能为空")
    private Date buyTime;

    /**
     * 产权
     */
    @NotNull(message = "产权不能为空")
    private Integer propertyRight;

    /**
     * 房屋类型
     */
    private HouseTypeEnum type;

    /**
     * 房屋图像
     */
    @TableField(exist = false)
    private List<HouseImage> list;

    /**
     * 房屋位置
     */
    @TableField(exist = false)
    private HouseLocation houseLocation;

}
