package xyz.ibenben.zhongdian.system.entity.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * 公告类型枚举类
 * 包括后台公告，前台公告等
 * 使用枚举类可以有效的节省代码行数，调整系统的耦合度
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
public enum MailTypeEnum {
    //类型 1后台公告 2前台公告 3后台私信 4前台私信
    BACKNOTICE(0, "后台公告"), FRONTNOTICE(1, "前台公告"), BACKPRIVATE(2, "后台私信 "), FRONTPRIVATE(3, "前台私信");

    /* Map对象 */
    private static final Map<Integer, MailTypeEnum> valueMap = new HashMap<>();

    static {
        for (MailTypeEnum mail : MailTypeEnum.values()) {
            valueMap.put(mail.getValue(), mail);
        }
    }

    /* 值 */
    @EnumValue
    private final int value;
    /* 文字 */
    private final String text;

    /**
     * 构造方法
     *
     * @param value
     * @param text
     */
    MailTypeEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * 获取值
     *
     * @param value 参数
     * @return 返回值
     */
    public static MailTypeEnum getByValue(int value) {
        MailTypeEnum result = valueMap.get(value);
        if (result == null) {
            throw new IllegalArgumentException("No element matches " + value);
        }
        return result;
    }

    /**
     * 获取文字
     *
     * @param text 参数
     * @return 返回值
     */
    public static MailTypeEnum getByText(String text) {
        for (MailTypeEnum e : values()) {
            if (e.getText().equals(text)) {
                return e;
            }
        }
        return null;
    }

}