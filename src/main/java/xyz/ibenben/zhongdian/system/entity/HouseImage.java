package xyz.ibenben.zhongdian.system.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.enums.ImageTypeEnum;

import java.io.Serializable;

/**
 * 房屋图像实体类
 * 记录该表记录了房屋主键，房屋图像路径等字段
 * 表名是house_image
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
@Setter
@ToString
public class HouseImage extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -5539985342398083744L;

    /**
     * 房屋主键
     */
    private Long houseId;

    /**
     * 房屋图像路径
     */
    private String houseImg;

    /**
     * 图像类型
     */
    private ImageTypeEnum type;

}
