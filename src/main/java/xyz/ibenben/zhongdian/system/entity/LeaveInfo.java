package xyz.ibenben.zhongdian.system.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.enums.LeaveTypeEnum;

import java.io.Serializable;

/**
 * 请假申请表实体类
 * 记录该表记录了姓名，请假天数等字段
 * 表名是leave_info
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
@Setter
@ToString
public class LeaveInfo extends BaseEntity implements Serializable {
    /**
     * 姓名
     */
    private String name;
    /**
     * 请假天数
     */
    private Integer days;
    /**
     * 请假类型
     */
    private LeaveTypeEnum type;
    /**
     * 请假原因
     */
    private String reason;
    /**
     * activiti的key
     */
    private String activitiKey;
}
