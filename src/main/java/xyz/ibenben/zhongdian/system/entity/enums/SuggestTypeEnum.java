package xyz.ibenben.zhongdian.system.entity.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * 聊天类型枚举类
 * 包括私聊，聊天室等
 * 使用枚举类可以有效的节省代码行数，调整系统的耦合度
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
public enum SuggestTypeEnum {
    OPINION(0, "意见"), PROPOSAL(1, "建议");

    /* Map对象 */
    private static final Map<Integer, SuggestTypeEnum> valueMap = new HashMap<>();

    static {
        for (SuggestTypeEnum mail : SuggestTypeEnum.values()) {
            valueMap.put(mail.getValue(), mail);
        }
    }

    /* 值 */
    @EnumValue
    private final int value;
    /* 文字 */
    private final String text;

    /**
     * 构造方法
     *
     * @param value
     * @param text
     */
    SuggestTypeEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * 获取值
     *
     * @param value 参数
     * @return 返回值
     */
    public static SuggestTypeEnum getByValue(int value) {
        SuggestTypeEnum result = valueMap.get(value);
        if (result == null) {
            throw new IllegalArgumentException("No element matches " + value);
        }
        return result;
    }

    /**
     * 获取文字
     *
     * @param text 参数
     * @return 返回值
     */
    public static SuggestTypeEnum getByText(String text) {
        for (SuggestTypeEnum e : values()) {
            if (e.getText().equals(text)) {
                return e;
            }
        }
        return null;
    }

}