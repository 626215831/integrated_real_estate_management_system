package xyz.ibenben.zhongdian.system.entity.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * 朝向枚举类
 * 包括南，北等
 * 使用枚举类可以有效的节省代码行数，调整系统的耦合度
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
public enum OrientationEnum {
    SOUTH(0, "南"), NORTH(1, "北"), WEST(2, "西"), EAST(3, "东"), NORTHEAST(4, "东北"), SOUTHEAST(5, "东南"), NORTHWEST(6, "西北"), SOUTHWEST(7, "西南");

    /* Map对象 */
    private static final Map<Integer, OrientationEnum> valueMap = new HashMap<>();

    static {
        for (OrientationEnum mail : OrientationEnum.values()) {
            valueMap.put(mail.getValue(), mail);
        }
    }

    /* 值 */
    @EnumValue
    private final int value;
    /* 文字 */
    private final String text;

    /**
     * 构造方法
     *
     * @param value
     * @param text
     */
    OrientationEnum(Integer value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * 获取值
     *
     * @param value 参数
     * @return 返回值
     */
    public static OrientationEnum getByValue(Integer value) {
        OrientationEnum result = valueMap.get(value);
        if (result == null) {
            throw new IllegalArgumentException("No element matches " + value);
        }
        return result;
    }

    /**
     * 获取文字
     *
     * @param text 参数
     * @return 返回值
     */
    public static OrientationEnum getByText(String text) {
        for (OrientationEnum e : values()) {
            if (e.getText().equals(text)) {
                return e;
            }
        }
        return null;
    }

}