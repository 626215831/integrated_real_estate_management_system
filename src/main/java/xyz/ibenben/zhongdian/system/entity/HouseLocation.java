package xyz.ibenben.zhongdian.system.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 房屋位置实体类
 * 位置该表记录了房屋的经纬度
 * 表名是house_location
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
@Setter
@ToString
public class HouseLocation extends BaseEntity implements Serializable {
    /**
     * 房屋主键
     */
    private Long houseId;

    /**
     * 经度
     */
    private String longitude;

    /**
     * 纬度
     */
    private String latitude;
}
