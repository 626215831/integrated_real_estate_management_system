package xyz.ibenben.zhongdian.system.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.enums.CompanyStatusEnum;
import xyz.ibenben.zhongdian.system.entity.enums.CompanyTypeEnum;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 公司信息实体类
 * 记录该表记录了公司名称，公司地址等字段
 * 表名是company_info
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
@Setter
@ToString
public class CompanyInfo extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 5441997212327447070L;

    /**
     * 公司名称
     */
    @NotEmpty(message = "公司名称不能为空")
    private String companyName;

    /**
     * 公司地址
     */
    @NotEmpty(message = "公司地址不能为空")
    private String companyAddress;

    /**
     * 法人
     */
    private String businessMan;

    /**
     * 联系电话
     */
    @NotNull(message = "联系电话不能为空")
    private String phone;

    /**
     * 公司类型 0-开发商 1-中介 2-网站 3-私人
     */
    @NotNull(message = "公司类型不能为空")
    private CompanyTypeEnum type;

    /**
     * 注册资本
     */
    private Integer registeredCapital;

    /**
     * 经营状态
     */
    @NotNull(message = "经营状态不能为空")
    private CompanyStatusEnum status;

    /**
     * 电子邮箱
     */
    @NotNull(message = "电子邮箱不能为空")
    private String email;

    /**
     * 成立日期
     */
    private Date createDate;

    /**
     * 企业网址
     */
    private String url;

    /**
     * 公司代码
     */
    private String companyCode;

    /**
     * 公司性质
     */
    private String companyNature;

    /**
     * 现公司地址
     */
    @NotNull(message = "现公司地址不能为空")
    private String nowAddress;

    /**
     * 是否包含分公司
     */
    private Integer subCompany;

    /**
     * 员工数量
     */
    private String staffNumber;

    /**
     * 经营范围
     */
    private String scope;

    /**
     * 所在省
     */
    @NotNull(message = "所在省不能为空")
    private Long province;

    /**
     * 所在市
     */
    @NotNull(message = "所在市不能为空")
    private Long city;

    /**
     * 所在区
     */
    @NotNull(message = "所在区不能为空")
    private Long region;

}
