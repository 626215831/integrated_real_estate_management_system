package xyz.ibenben.zhongdian.system.entity.sys;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 系统角色资源实体类（多对多关系表）
 * 记录该表记录了角色主键，资源主键等字段
 * 表名是sys_role_resources
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
@Setter
@ToString
public class SysRoleResources implements Serializable {
    private static final long serialVersionUID = -8559867942708057891L;

    /**
     * 角色主键
     */
    private Long roleId;

    /**
     * 资源主键
     */
    private Long resourcesId;

    @TableField(exist = false)
    private String resourcesIds;
}