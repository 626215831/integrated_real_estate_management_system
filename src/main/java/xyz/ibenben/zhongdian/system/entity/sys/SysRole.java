package xyz.ibenben.zhongdian.system.entity.sys;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.ibenben.zhongdian.system.entity.BaseEntity;
import xyz.ibenben.zhongdian.system.entity.CompanyInfo;

import java.io.Serializable;

/**
 * 系统角色实体类
 * 记录该表记录了角色描述，是否选择等字段
 * 表名是sys_role
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@ToString
@Setter
@Getter
public class SysRole extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -6140090613812307452L;

    /**
     * 角色描述
     */
    private String roleDesc;

    /**
     * 是否是公司用户
     */
    private Integer type;

    /**
     * 公司主键
     */
    private Long companyId;

    /**
     * 公司信息
     */
    @TableField(exist = false)
    private CompanyInfo companyInfo;

    /**
     * 是否选择
     */
    @TableField(exist = false)
    private Integer selected;

}