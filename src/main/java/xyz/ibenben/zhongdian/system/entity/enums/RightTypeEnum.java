package xyz.ibenben.zhongdian.system.entity.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * 权利类型枚举类
 * 包括典权，抵押权等
 * 使用枚举类可以有效的节省代码行数，调整系统的耦合度
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Getter
public enum RightTypeEnum {
    PAWNAGE(1, "典权"), MORTGAGE(2, "抵押权");

    /* Map对象 */
    private static final Map<Integer, RightTypeEnum> valueMap = new HashMap<>();

    static {
        for (RightTypeEnum mail : RightTypeEnum.values()) {
            valueMap.put(mail.getValue(), mail);
        }
    }

    /* 值 */
    @EnumValue
    private final int value;
    /* 文字 */
    private final String text;

    /**
     * 构造方法
     *
     * @param value
     * @param text
     */
    RightTypeEnum(int value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * 获取值
     *
     * @param value 参数
     * @return 返回值
     */
    public static RightTypeEnum getByValue(int value) {
        RightTypeEnum result = valueMap.get(value);
        if (result == null) {
            throw new IllegalArgumentException("No element matches " + value);
        }
        return result;
    }

    /**
     * 获取文字
     *
     * @param text 参数
     * @return 返回值
     */
    public static RightTypeEnum getByText(String text) {
        for (RightTypeEnum e : values()) {
            if (e.getText().equals(text)) {
                return e;
            }
        }
        return null;
    }

}