package xyz.ibenben.zhongdian.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import xyz.ibenben.zhongdian.system.entity.Log;

/**
 * 日志Mapper类
 * 提供了一些基本的服务
 * 记录日志到数据库
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface LogMapper extends BaseMapper<Log> {
    //日志
}