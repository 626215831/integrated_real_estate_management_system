package xyz.ibenben.zhongdian.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import xyz.ibenben.zhongdian.system.entity.HouseImage;
import xyz.ibenben.zhongdian.system.form.HouseImageForm;

/**
 * 房屋图像Mapper类
 * 提供了一些基本的服务。
 * 此类提供了所有系统里图像的处理功能
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface HouseImageMapper extends BaseMapper<HouseImage> {
    /**
     * 根据条件查询列表
     *
     * @param page           分页参数
     * @param houseImageForm 条件
     * @return 列表
     */
    IPage<HouseImage> findAll(Page<HouseImage> page, @Param("houseImage") HouseImageForm houseImageForm);
}