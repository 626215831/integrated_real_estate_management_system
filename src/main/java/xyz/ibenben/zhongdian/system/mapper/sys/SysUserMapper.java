package xyz.ibenben.zhongdian.system.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import xyz.ibenben.zhongdian.system.entity.sys.SysUser;
import xyz.ibenben.zhongdian.system.form.sys.SysUserForm;

/**
 * 系统用户Mapper类
 * 系统级用户所使用的，是权限系统中的一部分
 * 提供了一些基本的服务。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {
    /**
     * 根据条件查找列表
     *
     * @param page        分页参数
     * @param sysUserForm 条件
     * @return 列表
     */
    IPage<SysUser> findAll(Page<SysUser> page, @Param("user") SysUserForm sysUserForm);

    /**
     * 根据主键查找记录
     *
     * @param id 主键
     * @return 记录
     */
    SysUser findByKey(@Param("id") Long id);

    /**
     * 根据用户名称获取用户信息
     *
     * @param username 用户名称
     * @return 用户信息
     */
    SysUser selectUserWithRoleByUsername(@Param("username") String username);

    /**
     * 根据主键删除用户角色表里的记录
     *
     * @param id 主键
     * @return 数量
     */
    int deleteUserRoleById(Object id);
}