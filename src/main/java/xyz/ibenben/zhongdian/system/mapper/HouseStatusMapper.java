package xyz.ibenben.zhongdian.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import xyz.ibenben.zhongdian.system.entity.HouseStatus;
import xyz.ibenben.zhongdian.system.form.HouseStatusForm;

/**
 * 房屋状态Mapper类
 * 提供了一些基本的服务。
 * 属于产权下的一个分支
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface HouseStatusMapper extends BaseMapper<HouseStatus> {
    /**
     * 根据条件查询列表
     *
     * @param page            分页参数
     * @param houseStatusForm 条件
     * @return 列表
     */
    IPage<HouseStatus> findAll(Page<HouseStatus> page, @Param("houseStatus") HouseStatusForm houseStatusForm);
}