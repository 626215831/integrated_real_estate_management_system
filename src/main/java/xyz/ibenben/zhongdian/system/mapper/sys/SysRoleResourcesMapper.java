package xyz.ibenben.zhongdian.system.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import xyz.ibenben.zhongdian.system.entity.sys.SysRoleResources;

/**
 * 系统角色资源Mapper类
 * 系统级角色管理所使用的，是权限系统中的一部分
 * 提供了一些基本的服务。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface SysRoleResourcesMapper extends BaseMapper<SysRoleResources> {
    //系统角色资源
}