package xyz.ibenben.zhongdian.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import xyz.ibenben.zhongdian.system.entity.DiscountInfo;
import xyz.ibenben.zhongdian.system.form.DiscountInfoForm;

/**
 * 折扣信息Mapper类
 * 提供了一些基本的服务，如获取所有房屋折扣等方法。
 * 是此系统的主要服务类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface DiscountInfoMapper extends BaseMapper<DiscountInfo> {
    /**
     * 根据条件查询列表
     *
     * @param page             分页参数
     * @param discountInfoForm 条件
     * @return 列表
     */
    IPage<DiscountInfo> findAll(Page<DiscountInfo> page, @Param("discount") DiscountInfoForm discountInfoForm);
}