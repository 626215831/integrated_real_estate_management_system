package xyz.ibenben.zhongdian.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import xyz.ibenben.zhongdian.system.entity.Reminder;
import xyz.ibenben.zhongdian.system.form.ReminderForm;

import java.util.Date;
import java.util.List;

/**
 * 系统提醒记录Mapper类
 * 提供了一些基本的服务，如根据条件查询列表等方法。
 * 是用户在系统提醒时所需要操作的类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface ReminderMapper extends BaseMapper<Reminder> {

    /**
     * 根据条件查询列表
     *
     * @param page         分页参数
     * @param reminderForm 条件
     * @return 列表
     */
    IPage<Reminder> findAll(Page<Reminder> page, @Param("reminder") ReminderForm reminderForm);

    /**
     * 根据主键获取记录
     *
     * @param id 主键
     * @return 记录
     */
    Reminder findByKey(@Param("id") Long id);

    /**
     * 根据时间获取记录列表
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return 记录列表
     */
    List<Reminder> findByTime(@Param("startTime") Date startTime, @Param("endTime") Date endTime);
}