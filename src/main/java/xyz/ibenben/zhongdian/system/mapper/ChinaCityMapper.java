package xyz.ibenben.zhongdian.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import xyz.ibenben.zhongdian.system.entity.ChinaCity;

/**
 * 城市Mapper类
 * 提供了一些基本的服务，如获取指定数量的记录列表等方法。
 * 是用户在互相聊天时所需要操作的类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface ChinaCityMapper extends BaseMapper<ChinaCity> {

}