package xyz.ibenben.zhongdian.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import xyz.ibenben.zhongdian.system.entity.InnerMailRecord;

/**
 * 站内信记录Mapper类
 * 提供了一些基本的服务.
 * 主要用于用户之间通信使用此类来维护
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface InnerMailRecordMapper extends BaseMapper<InnerMailRecord> {
    //站内信记录
}