package xyz.ibenben.zhongdian.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import xyz.ibenben.zhongdian.system.entity.CompanyInfo;
import xyz.ibenben.zhongdian.system.form.CompanyInfoForm;

/**
 * 公司信息Mapper类
 * 提供了一些基本的服务，如获取所有公司信息等方法。
 * 是此系统的主要服务类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface CompanyInfoMapper extends BaseMapper<CompanyInfo> {
    /**
     * 根据主键查找公司信息
     *
     * @param id 公司主键
     * @return 公司信息
     */
    CompanyInfo findByKey(@Param("id") Long id);

    /**
     * 根据条件查询列表
     *
     * @param page            分页参数
     * @param companyInfoForm 条件
     * @return 列表
     */
    IPage<CompanyInfo> findAll(Page<CompanyInfo> page, @Param("company") CompanyInfoForm companyInfoForm);
}