package xyz.ibenben.zhongdian.system.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import xyz.ibenben.zhongdian.system.entity.sys.SysUserRole;

import java.util.List;

/**
 * 系统用户角色Mapper类
 * 系统级用户所使用的，是权限系统中的一部分
 * 提供了一些基本的服务，如根据角色主键获取用户主键等方法。
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {
    /**
     * 根据角色主键获取用户主键
     *
     * @param roleId 角色主键
     * @return 用户主键列表
     */
    List<Long> findUserIdByRoleId(@Param("roleId") Long roleId);

    /**
     * 根据用户主键获取角色主键
     *
     * @param userId 用户主键
     * @return 角色主键列表
     */
    List<Long> findRoleIdByUserId(@Param("userId") Long userId);

}