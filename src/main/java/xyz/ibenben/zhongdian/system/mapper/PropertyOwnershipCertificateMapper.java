package xyz.ibenben.zhongdian.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import xyz.ibenben.zhongdian.system.entity.PropertyOwnershipCertificate;
import xyz.ibenben.zhongdian.system.form.PropertyOwnershipCertificateForm;

/**
 * 产权Mapper类
 * 提供了一些基本的服务
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface PropertyOwnershipCertificateMapper extends BaseMapper<PropertyOwnershipCertificate> {
    /**
     * 根据条件查询列表
     *
     * @param page                             分页参数
     * @param propertyOwnershipCertificateForm 条件
     * @return 里恩
     */
    IPage<PropertyOwnershipCertificate> findAll(Page<PropertyOwnershipCertificate> page,
                                                @Param("certificate") PropertyOwnershipCertificateForm propertyOwnershipCertificateForm);
}