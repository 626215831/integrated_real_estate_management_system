package xyz.ibenben.zhongdian.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import xyz.ibenben.zhongdian.system.entity.LeaveInfo;
import xyz.ibenben.zhongdian.system.form.LeaveInfoForm;

/**
 * 请假记录Mapper类
 * 提供了一些基本的服务，如获取指定数量的记录列表等方法。
 * 是用户在请假时所需要操作的类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface LeaveInfoMapper extends BaseMapper<LeaveInfo> {
    /**
     * 根据条件查询列表
     *
     * @param page          分页条件
     * @param leaveInfoForm 条件
     * @return 列表
     */
    IPage<LeaveInfo> findAll(Page<LeaveInfo> page, @Param("leave") LeaveInfoForm leaveInfoForm);

}