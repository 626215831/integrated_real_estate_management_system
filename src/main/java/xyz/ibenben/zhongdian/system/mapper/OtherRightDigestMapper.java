package xyz.ibenben.zhongdian.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import xyz.ibenben.zhongdian.system.entity.OtherRightDigest;
import xyz.ibenben.zhongdian.system.form.OtherRightDigestForm;

/**
 * 他项权利摘要Mapper类
 * 提供了一些基本的服务。
 * 属于产权下的一个分支
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface OtherRightDigestMapper extends BaseMapper<OtherRightDigest> {
    /**
     * 根据条件查询列表
     *
     * @param page                 分页参数
     * @param otherRightDigestForm 条件
     * @return 列表
     */
    IPage<OtherRightDigest> findAll(Page<OtherRightDigest> page, @Param("other") OtherRightDigestForm otherRightDigestForm);
}