package xyz.ibenben.zhongdian.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import xyz.ibenben.zhongdian.system.entity.HouseInfo;
import xyz.ibenben.zhongdian.system.form.HouseInfoForm;

import java.util.List;
import java.util.Map;

/**
 * 房屋信息Mapper类
 * 提供了一些基本的服务，如获取所有房屋信息等方法。
 * 是此系统的主要服务类
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Mapper
public interface HouseInfoMapper extends BaseMapper<HouseInfo> {
    /**
     * 获取所有房屋信息
     *
     * @return 返回值
     */
    List<Map<String, Object>> getHouseInfoList();

    /**
     * 根据小区查询符合的记录列表
     *
     * @param map 参数
     * @return 返回值
     */
    List<HouseInfo> getHouseInfoListByPlot(Map<String, Object> map);

    /**
     * 根据主键获取位置信息及中文信息
     *
     * @param houseId 房屋主键
     * @return 带中文位置信息的实体类
     */
    HouseInfo findWithLocation(@Param("houseId") Long houseId);

    /**
     * 根据条件查找列表
     *
     * @param page          分页参数
     * @param houseInfoForm 条件
     * @return 列表
     */
    IPage<HouseInfo> findAll(Page<HouseInfo> page, @Param("houseInfo") HouseInfoForm houseInfoForm);

    /**
     * 获取房屋地图信息
     *
     * @return 房屋地图信息
     */
    List<Map<String, Object>> getWholeChinaInfo();
}