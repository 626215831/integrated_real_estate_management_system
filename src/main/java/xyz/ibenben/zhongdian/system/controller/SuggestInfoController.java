package xyz.ibenben.zhongdian.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import xyz.ibenben.zhongdian.common.annotation.SystemControllerLog;
import xyz.ibenben.zhongdian.common.constants.Constants;
import xyz.ibenben.zhongdian.system.entity.SuggestInfo;
import xyz.ibenben.zhongdian.system.form.SuggestInfoForm;
import xyz.ibenben.zhongdian.system.service.SuggestInfoService;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 意见及建议控制类
 * 提供了一些基本的服务，如初始化、获取意见及建议列表等方法。
 * spring mvc思想 通过controller来调用service里的方法，
 * service里的方法再通过调用Dao来实现对数据的操作
 * 返回值统统是String类型的返回页面有统一的安排方便代码提取
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Slf4j
@Controller
@SessionAttributes("suggestInfoForm")
@RequestMapping("/suggestInfo")
public class SuggestInfoController extends BaseController {
    /**
     * 实体名称
     */
    private static final String ENTITYNAME = "suggestInfo";

    @Resource
    private SuggestInfoService suggestInfoService;

    /**
     * 初始化
     *
     * @param map             参数
     * @param suggestInfoForm 条件参数
     */
    @ModelAttribute
    public void init(Model map, @ModelAttribute SuggestInfoForm suggestInfoForm) {
        SuggestInfoForm newSuggestInfoForm = new SuggestInfoForm();
        if (suggestInfoForm != null) {
            newSuggestInfoForm = suggestInfoForm;
        }
        map.addAttribute(String.format(Constants.FORMNAME, ENTITYNAME), newSuggestInfoForm);
    }

    /**
     * 获取意见及建议列表
     *
     * @param map             参数
     * @param suggestInfoForm 条件参数
     * @return 页面
     */
    @RequestMapping(value = "/getAll")
    @SystemControllerLog(description = "获取意见及建议列表")
    public String getAll(Model map, @ModelAttribute SuggestInfoForm suggestInfoForm) {
        log.info("getAll param SuggestInfoForm: {}", suggestInfoForm.toString());
        map.addAttribute(String.format(Constants.FORMNAME, ENTITYNAME), suggestInfoForm);
        Page<SuggestInfo> page = new Page<>(suggestInfoForm.getPageIndex(), suggestInfoForm.getPageSize());
        IPage<SuggestInfo> result = suggestInfoService.findAll(page, suggestInfoForm);
        map.addAttribute(Constants.PI, result);
        return String.format(Constants.LISTPAGE, ENTITYNAME);
    }

    /**
     * 删除意见及建议记录
     *
     * @param id 主键
     * @return 页面
     */
    @RequestMapping(value = "/delete")
    @SystemControllerLog(description = "删除意见及建议记录")
    public String delete(@RequestParam("id") Long id, SessionStatus status) {
        suggestInfoService.removeById(id);
        status.setComplete();
        return String.format(Constants.REDIRECT, ENTITYNAME);
    }

    /**
     * 删除意见及建议列表
     *
     * @param suggestInfoForm 条件参数
     * @return 页面
     */
    @RequestMapping("/deleteAll")
    @SystemControllerLog(description = "删除意见及建议列表")
    public String deleteAll(@ModelAttribute SuggestInfoForm suggestInfoForm,
                            SessionStatus status) {
        if (suggestInfoForm.getSelectIds() != null) {
            String[] ids = suggestInfoForm.getSelectIds().split(Constants.COMMA);
            for (String id : ids) {
                suggestInfoService.removeById(Long.parseLong(id));
            }
        }
        status.setComplete();
        return String.format(Constants.REDIRECT, ENTITYNAME);
    }

    /**
     * 打开意见及建议添加页面
     *
     * @param map 参数
     * @return 页面
     */
    @GetMapping("/add")
    @SystemControllerLog(description = "打开意见及建议添加页面")
    public String openAddPage(Model map) {
        map.addAttribute(ENTITYNAME, new SuggestInfo());
        return String.format(Constants.ADDPAGE, ENTITYNAME);
    }

    /**
     * 保存意见及建议添加记录
     *
     * @param suggestInfo   实体
     * @param bindingResult 校验
     * @return 页面
     */
    @PostMapping("/add")
    @SystemControllerLog(description = "保存意见及建议添加记录")
    public String postAddMessage(@Valid @ModelAttribute(ENTITYNAME) SuggestInfo suggestInfo,
                                 BindingResult bindingResult, SessionStatus status) {
        if (!bindingResult.hasErrors()) {
            suggestInfoService.save(suggestInfo);
            status.setComplete();
            return String.format(Constants.REDIRECT, ENTITYNAME);
        }
        return String.format(Constants.ADDPAGE, ENTITYNAME);
    }

    /**
     * 浏览意见及建议记录
     *
     * @param map             参数
     * @param id              主键
     * @param suggestInfoForm 条件参数
     * @return 页面
     */
    @GetMapping("/view")
    @SystemControllerLog(description = "浏览意见及建议记录")
    public String openViewPage(Model map, @RequestParam("id") Long id,
                               @ModelAttribute SuggestInfoForm suggestInfoForm) {
        SuggestInfo info = suggestInfoService.findByKey(id);
        map.addAttribute(ENTITYNAME, info);
        return String.format(Constants.VIEWPAGE, ENTITYNAME);
    }

}

