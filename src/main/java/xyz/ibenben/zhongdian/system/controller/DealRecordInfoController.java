package xyz.ibenben.zhongdian.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import xyz.ibenben.zhongdian.common.annotation.SystemControllerLog;
import xyz.ibenben.zhongdian.common.constants.Constants;
import xyz.ibenben.zhongdian.system.entity.DealRecordInfo;
import xyz.ibenben.zhongdian.system.entity.sys.SysUser;
import xyz.ibenben.zhongdian.system.form.DealRecordInfoForm;
import xyz.ibenben.zhongdian.system.service.DealRecordInfoService;
import xyz.ibenben.zhongdian.system.service.HouseInfoService;
import xyz.ibenben.zhongdian.system.service.impl.RedisService;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

/**
 * 成交记录信息控制类
 * 提供了一些基本的服务，如初始化、获取成交记录信息列表等方法。
 * spring mvc思想 通过controller来调用service里的方法，
 * service里的方法再通过调用Dao来实现对数据的操作
 * 返回值统统是String类型的返回页面有统一的安排方便代码提取
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Slf4j
@Controller
@SessionAttributes("dealRecordInfoForm")
@RequestMapping("/dealRecordInfo")
public class DealRecordInfoController extends BaseController {
    /**
     * 实体名称
     */
    private static final String ENTITYNAME = "dealRecordInfo";

    @Resource
    private DealRecordInfoService dealRecordInfoService;
    @Resource
    private RedisService redisService;
    @Resource
    private HouseInfoService houseInfoService;

    /**
     * 初始化
     *
     * @param map                参数
     * @param dealRecordInfoForm 条件参数
     */
    @ModelAttribute
    public void init(Model map, @ModelAttribute DealRecordInfoForm dealRecordInfoForm, HttpSession session) {
        map.addAttribute("houseInfoList", houseInfoService.list());
        map.addAttribute("provinceList", redisService.get("provinceList"));
        map.addAttribute("cityList", redisService.get("cityList"));
        map.addAttribute("regionList", redisService.get("regionList"));
        DealRecordInfoForm newDealRecordInfoForm = new DealRecordInfoForm();
        if (dealRecordInfoForm != null) {
            newDealRecordInfoForm = dealRecordInfoForm;
        }
        SysUser user = (SysUser) session.getAttribute(Constants.SESSION);
        if (user != null && user.getSysRole() != null) {
            newDealRecordInfoForm.setCompanyId(user.getSysRole().getCompanyId());
        }
        map.addAttribute(String.format(Constants.FORMNAME, ENTITYNAME), newDealRecordInfoForm);
    }

    /**
     * 获取成交记录信息列表
     *
     * @param map                参数
     * @param dealRecordInfoForm 条件参数
     * @return 页面
     */
    @RequestMapping(value = "/getAll")
    @SystemControllerLog(description = "获取成交记录信息列表")
    public String getAll(Model map, @ModelAttribute DealRecordInfoForm dealRecordInfoForm) {
        log.info("getAll param DealRecordInfoForm: {}", dealRecordInfoForm.toString());
        map.addAttribute(String.format(Constants.FORMNAME, ENTITYNAME), dealRecordInfoForm);
        Page<DealRecordInfo> page = new Page<>(dealRecordInfoForm.getPageIndex(), dealRecordInfoForm.getPageSize());
        IPage<DealRecordInfo> result = dealRecordInfoService.findAll(page, dealRecordInfoForm);
        map.addAttribute(Constants.PI, result);
        return String.format(Constants.LISTPAGE, ENTITYNAME);
    }

    /**
     * 删除成交记录信息记录
     *
     * @param id 主键
     * @return 页面
     */
    @RequestMapping(value = "/delete")
    @SystemControllerLog(description = "删除成交记录信息记录")
    public String delete(@RequestParam("id") Long id, SessionStatus status) {
        dealRecordInfoService.removeById(id);
        status.setComplete();
        return String.format(Constants.REDIRECT, ENTITYNAME);
    }

    /**
     * 删除成交记录信息列表
     *
     * @param dealRecordInfoForm 条件参数
     * @return 页面
     */
    @RequestMapping("/deleteAll")
    @SystemControllerLog(description = "删除成交记录信息列表")
    public String deleteAll(@ModelAttribute DealRecordInfoForm dealRecordInfoForm, SessionStatus status) {
        if (dealRecordInfoForm.getSelectIds() != null) {
            String[] ids = dealRecordInfoForm.getSelectIds().split(Constants.COMMA);
            for (String id : ids) {
                dealRecordInfoService.removeById(Long.parseLong(id));
            }
        }
        status.setComplete();
        return String.format(Constants.REDIRECT, ENTITYNAME);
    }

    /**
     * 打开成交记录信息添加页面
     *
     * @param map 参数
     * @return 页面
     */
    @GetMapping("/add")
    @SystemControllerLog(description = "打开成交记录信息添加页面")
    public String openAddPage(Model map) {
        map.addAttribute(ENTITYNAME, new DealRecordInfo());
        return String.format(Constants.ADDPAGE, ENTITYNAME);
    }

    /**
     * 保存成交记录信息添加记录
     *
     * @param dealRecordInfo 实体
     * @param bindingResult  校验
     * @return 页面
     */
    @PostMapping("/add")
    @SystemControllerLog(description = "保存成交记录信息添加记录")
    public String postAddMessage(@Valid @ModelAttribute(ENTITYNAME) DealRecordInfo dealRecordInfo,
                                 BindingResult bindingResult, SessionStatus status) {
        if (!bindingResult.hasErrors()) {
            dealRecordInfoService.save(dealRecordInfo);
            status.setComplete();
            return String.format(Constants.REDIRECT, ENTITYNAME);
        } else {
            return String.format(Constants.ADDPAGE, ENTITYNAME);
        }
    }

    /**
     * 打开成交记录信息修改页面
     *
     * @param map 参数
     * @param id  主键
     * @return 页面
     */
    @GetMapping("/edit")
    @SystemControllerLog(description = "打开成交记录信息修改页面")
    public String openEditPage(Model map, @RequestParam("id") Long id) {
        map.addAttribute(ENTITYNAME, dealRecordInfoService.findByKey(id));
        return String.format(Constants.EDITPAGE, ENTITYNAME);
    }

    /**
     * 更新成交记录信息修改记录
     *
     * @param dealRecordInfo 实体
     * @param bindingResult  校验
     * @return 页面
     */
    @PostMapping("/edit")
    @SystemControllerLog(description = "更新成交记录信息修改记录")
    public String postEditMessage(@Valid @ModelAttribute(ENTITYNAME) DealRecordInfo dealRecordInfo,
                                  BindingResult bindingResult, SessionStatus status) {
        if (!bindingResult.hasErrors()) {
            dealRecordInfoService.updateById(dealRecordInfo);
            status.setComplete();
            return String.format(Constants.REDIRECT, ENTITYNAME);
        } else {
            return String.format(Constants.EDITPAGE, ENTITYNAME);
        }
    }

    /**
     * 浏览成交记录信息记录
     *
     * @param map                参数
     * @param id                 主键
     * @param dealRecordInfoForm 条件参数
     * @return 页面
     */
    @GetMapping("/view")
    @SystemControllerLog(description = "浏览成交记录信息记录")
    public String openViewPage(Model map, @RequestParam("id") Long id,
                               @ModelAttribute DealRecordInfoForm dealRecordInfoForm) {
        DealRecordInfo info = dealRecordInfoService.findByKey(id);
        map.addAttribute(ENTITYNAME, info);
        return String.format(Constants.VIEWPAGE, ENTITYNAME);
    }

}

