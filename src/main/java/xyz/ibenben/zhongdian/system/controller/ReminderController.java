package xyz.ibenben.zhongdian.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import xyz.ibenben.zhongdian.common.annotation.SystemControllerLog;
import xyz.ibenben.zhongdian.common.constants.Constants;
import xyz.ibenben.zhongdian.system.entity.Reminder;
import xyz.ibenben.zhongdian.system.form.ReminderForm;
import xyz.ibenben.zhongdian.system.service.ReminderService;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 系统提醒控制类
 * 提供了一些基本的服务，如初始化、获取系统提醒列表等方法。
 * spring mvc思想 通过controller来调用service里的方法，
 * service里的方法再通过调用Dao来实现对数据的操作
 * 返回值统统是String类型的返回页面有统一的安排方便代码提取
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Slf4j
@Controller
@SessionAttributes("reminderForm")
@RequestMapping("/reminder")
public class ReminderController extends BaseController {
    /**
     * 实体名称
     */
    private static final String ENTITYNAME = "reminder";

    @Resource
    private ReminderService reminderService;

    /**
     * 初始化
     *
     * @param map          参数
     * @param reminderForm 条件参数
     */
    @ModelAttribute
    public void init(Model map, @ModelAttribute ReminderForm reminderForm) {
        ReminderForm newReminderForm = new ReminderForm();
        if (reminderForm != null) {
            newReminderForm = reminderForm;
        }
        map.addAttribute(String.format(Constants.FORMNAME, ENTITYNAME), newReminderForm);
    }

    /**
     * 获取系统提醒列表
     *
     * @param map          参数
     * @param reminderForm 条件参数
     * @return 页面
     */
    @RequestMapping(value = "/getAll")
    @SystemControllerLog(description = "获取系统提醒列表")
    public String getAll(Model map, @ModelAttribute ReminderForm reminderForm) {
        log.info("getAll param ReminderForm: {}", reminderForm.toString());
        map.addAttribute(String.format(Constants.FORMNAME, ENTITYNAME), reminderForm);
        Page<Reminder> page = new Page<>(reminderForm.getPageIndex(), reminderForm.getPageSize());
        IPage<Reminder> result = reminderService.findAll(page, reminderForm);
        map.addAttribute(Constants.PI, result);
        return String.format(Constants.LISTPAGE, ENTITYNAME);
    }

    /**
     * 删除系统提醒记录
     *
     * @param id 主键
     * @return 页面
     */
    @RequestMapping(value = "/delete")
    @SystemControllerLog(description = "删除系统提醒记录")
    public String delete(@RequestParam("id") Long id, SessionStatus status) {
        reminderService.removeById(id);
        status.setComplete();
        return String.format(Constants.REDIRECT, ENTITYNAME);
    }

    /**
     * 删除系统提醒列表
     *
     * @param reminderForm 条件参数
     * @return 页面
     */
    @RequestMapping("/deleteAll")
    @SystemControllerLog(description = "删除系统提醒列表")
    public String deleteAll(@ModelAttribute ReminderForm reminderForm,
                            SessionStatus status) {
        if (reminderForm.getSelectIds() != null) {
            String[] ids = reminderForm.getSelectIds().split(Constants.COMMA);
            for (String id : ids) {
                reminderService.removeById(Long.parseLong(id));
            }
        }
        status.setComplete();
        return String.format(Constants.REDIRECT, ENTITYNAME);
    }

    /**
     * 打开系统提醒添加页面
     *
     * @param map 参数
     * @return 页面
     */
    @GetMapping("/add")
    @SystemControllerLog(description = "打开系统提醒添加页面")
    public String openAddPage(Model map) {
        map.addAttribute(ENTITYNAME, new Reminder());
        return String.format(Constants.ADDPAGE, ENTITYNAME);
    }

    /**
     * 保存系统提醒添加记录
     *
     * @param reminder      实体
     * @param bindingResult 校验
     * @return 页面
     */
    @PostMapping("/add")
    @SystemControllerLog(description = "保存系统提醒添加记录")
    public String postAddMessage(@Valid @ModelAttribute(ENTITYNAME) Reminder reminder,
                                 BindingResult bindingResult, SessionStatus status) {
        if (!bindingResult.hasErrors()) {
            reminderService.save(reminder);
            status.setComplete();
            return String.format(Constants.REDIRECT, ENTITYNAME);
        }
        return String.format(Constants.ADDPAGE, ENTITYNAME);
    }

    /**
     * 打开系统提醒修改页面
     *
     * @param map 参数
     * @param id  主键
     * @return 页面
     */
    @GetMapping("/edit")
    @SystemControllerLog(description = "打开系统提醒修改页面")
    public String openEditPage(Model map, @RequestParam("id") Long id) {
        map.addAttribute(ENTITYNAME, reminderService.findByKey(id));
        return String.format(Constants.EDITPAGE, ENTITYNAME);
    }

    /**
     * 更新系统提醒修改记录
     *
     * @param reminder      实体
     * @param bindingResult 校验
     * @return 页面
     */
    @PostMapping("/edit")
    @SystemControllerLog(description = "更新系统提醒修改记录")
    public String postEditMessage(@Valid @ModelAttribute(ENTITYNAME) Reminder reminder,
                                  BindingResult bindingResult, SessionStatus status) {
        if (!bindingResult.hasErrors()) {
            reminderService.updateById(reminder);
            status.setComplete();
            return String.format(Constants.REDIRECT, ENTITYNAME);
        } else {
            return String.format(Constants.EDITPAGE, ENTITYNAME);
        }
    }

    /**
     * 浏览系统提醒记录
     *
     * @param map          参数
     * @param id           主键
     * @param reminderForm 条件参数
     * @return 页面
     */
    @GetMapping("/view")
    @SystemControllerLog(description = "浏览系统提醒记录")
    public String openViewPage(Model map, @RequestParam("id") Long id,
                               @ModelAttribute ReminderForm reminderForm) {
        Reminder info = reminderService.findByKey(id);
        map.addAttribute(ENTITYNAME, info);
        return String.format(Constants.VIEWPAGE, ENTITYNAME);
    }

}

