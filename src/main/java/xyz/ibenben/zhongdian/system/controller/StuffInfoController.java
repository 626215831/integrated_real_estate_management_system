package xyz.ibenben.zhongdian.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import xyz.ibenben.zhongdian.common.annotation.SystemControllerLog;
import xyz.ibenben.zhongdian.common.constants.Constants;
import xyz.ibenben.zhongdian.system.entity.StuffInfo;
import xyz.ibenben.zhongdian.system.entity.sys.SysUser;
import xyz.ibenben.zhongdian.system.form.StuffInfoForm;
import xyz.ibenben.zhongdian.system.service.RentingHouseService;
import xyz.ibenben.zhongdian.system.service.StuffInfoService;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

/**
 * 租赁物品控制类
 * 提供了一些基本的服务，如初始化、获取租赁物品列表等方法。
 * spring mvc思想 通过controller来调用service里的方法，
 * service里的方法再通过调用Dao来实现对数据的操作
 * 返回值统统是String类型的返回页面有统一的安排方便代码提取
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Slf4j
@Controller
@SessionAttributes("stuffInfoForm")
@RequestMapping("/stuffInfo")
public class StuffInfoController extends BaseController {
    /**
     * 实体名称
     */
    private static final String ENTITYNAME = "stuffInfo";

    @Resource
    private StuffInfoService stuffInfoService;
    @Resource
    private RentingHouseService rentingHouseService;

    /**
     * 初始化
     *
     * @param map           参数
     * @param stuffInfoForm 条件参数
     */
    @ModelAttribute
    public void init(Model map, @ModelAttribute StuffInfoForm stuffInfoForm, HttpSession session) {
        StuffInfoForm newStuffInfoForm = new StuffInfoForm();
        if (stuffInfoForm != null) {
            newStuffInfoForm = stuffInfoForm;
        }
        SysUser user = (SysUser) session.getAttribute(Constants.SESSION);
        if (user != null && user.getSysRole() != null) {
            newStuffInfoForm.setCompanyId(user.getSysRole().getCompanyId());
        }
        map.addAttribute(String.format(Constants.FORMNAME, ENTITYNAME), newStuffInfoForm);
    }

    /**
     * 获取租赁物品信息列表
     *
     * @param map           参数
     * @param stuffInfoForm 条件参数
     * @return 页面
     */
    @RequestMapping(value = "/getAll")
    @SystemControllerLog(description = "获取租赁物品信息列表")
    public String getAll(Model map, @ModelAttribute StuffInfoForm stuffInfoForm) {
        log.info("getAll param StuffInfoForm: {}", stuffInfoForm.toString());
        map.addAttribute(String.format(Constants.FORMNAME, ENTITYNAME), stuffInfoForm);
        Page<StuffInfo> page = new Page<>(stuffInfoForm.getPageIndex(), stuffInfoForm.getPageSize());
        IPage<StuffInfo> result = stuffInfoService.findAll(page, stuffInfoForm);
        map.addAttribute(Constants.PI, result);
        return String.format(Constants.LISTPAGE, ENTITYNAME);
    }

    /**
     * 删除租赁物品信息记录
     *
     * @param id 主键
     * @return 页面
     */
    @RequestMapping(value = "/delete")
    @SystemControllerLog(description = "删除租赁物品信息记录")
    public String delete(@RequestParam("id") Long id, SessionStatus status) {
        stuffInfoService.removeById(id);
        status.setComplete();
        return String.format(Constants.REDIRECT, ENTITYNAME);
    }

    /**
     * 删除租赁物品信息列表
     *
     * @param stuffInfoForm 条件参数
     * @return 页面
     */
    @RequestMapping("/deleteAll")
    @SystemControllerLog(description = "删除租赁物品信息列表")
    public String deleteAll(@ModelAttribute StuffInfoForm stuffInfoForm,
                            SessionStatus status) {
        if (stuffInfoForm.getSelectIds() != null) {
            String[] ids = stuffInfoForm.getSelectIds().split(Constants.COMMA);
            for (String id : ids) {
                stuffInfoService.removeById(Long.parseLong(id));
            }
        }
        status.setComplete();
        return String.format(Constants.REDIRECT, ENTITYNAME);
    }

    /**
     * 打开租赁物品信息添加页面
     *
     * @param map 参数
     * @return 页面
     */
    @GetMapping("/add")
    @SystemControllerLog(description = "打开租赁物品信息添加页面")
    public String openAddPage(Model map, @RequestParam("id") Long id) {
        StuffInfo stuffInfo = new StuffInfo();
        stuffInfo.setRentingId(id);
        map.addAttribute(ENTITYNAME, stuffInfo);
        map.addAttribute("rentingHouse", rentingHouseService.getById(id));
        return String.format(Constants.ADDPAGE, ENTITYNAME);
    }

    /**
     * 保存租赁物品信息添加记录
     *
     * @param stuffInfo     实体
     * @param bindingResult 校验
     * @return 页面
     */
    @PostMapping("/add")
    @SystemControllerLog(description = "保存租赁物品信息添加记录")
    public String postAddMessage(@Valid @ModelAttribute(ENTITYNAME) StuffInfo stuffInfo,
                                 BindingResult bindingResult,
                                 Model map, SessionStatus status) {
        if (!bindingResult.hasErrors()) {
            stuffInfoService.save(stuffInfo);
            status.setComplete();
            return String.format(Constants.REDIRECT, ENTITYNAME);
        } else {
            map.addAttribute("rentingHouse", rentingHouseService.getById(stuffInfo.getRentingId()));
            return String.format(Constants.ADDPAGE, ENTITYNAME);
        }
    }

    /**
     * 打开租赁物品信息修改页面
     *
     * @param map 参数
     * @param id  主键
     * @return 页面
     */
    @GetMapping("/edit")
    @SystemControllerLog(description = "打开租赁物品信息修改页面")
    public String openEditPage(Model map, @RequestParam("id") Long id) {
        map.addAttribute(ENTITYNAME, stuffInfoService.getById(id));
        return String.format(Constants.EDITPAGE, ENTITYNAME);
    }

    /**
     * 更新租赁物品信息修改记录
     *
     * @param stuffInfo     实体
     * @param bindingResult 校验
     * @return 页面
     */
    @PostMapping("/edit")
    @SystemControllerLog(description = "更新租赁物品信息修改记录")
    public String postEditMessage(@Valid @ModelAttribute(ENTITYNAME) StuffInfo stuffInfo,
                                  BindingResult bindingResult, SessionStatus status) {
        if (!bindingResult.hasErrors()) {
            stuffInfoService.updateById(stuffInfo);
            status.setComplete();
            return String.format(Constants.REDIRECT, ENTITYNAME);
        } else {
            return String.format(Constants.EDITPAGE, ENTITYNAME);
        }
    }

    /**
     * 浏览租赁物品信息记录
     *
     * @param map           参数
     * @param id            主键
     * @param stuffInfoForm 条件参数
     * @return 页面
     */
    @GetMapping("/view")
    @SystemControllerLog(description = "浏览租赁物品信息记录")
    public String openViewPage(Model map, @RequestParam("id") Long id,
                               @ModelAttribute StuffInfoForm stuffInfoForm) {
        StuffInfo info = stuffInfoService.findByKey(id);
        map.addAttribute(ENTITYNAME, info);
        return String.format(Constants.VIEWPAGE, ENTITYNAME);
    }

}

