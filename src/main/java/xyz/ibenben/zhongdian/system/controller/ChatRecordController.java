package xyz.ibenben.zhongdian.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import xyz.ibenben.zhongdian.common.annotation.SystemControllerLog;
import xyz.ibenben.zhongdian.common.constants.Constants;
import xyz.ibenben.zhongdian.system.entity.ChatRecord;
import xyz.ibenben.zhongdian.system.form.ChatRecordForm;
import xyz.ibenben.zhongdian.system.service.ChatRecordService;
import xyz.ibenben.zhongdian.system.service.impl.RedisService;

import javax.annotation.Resource;

/**
 * 聊天记录控制类
 * 提供了一些基本的服务，如初始化、获取聊天记录列表等方法。
 * spring mvc思想 通过controller来调用service里的方法，
 * service里的方法再通过调用Dao来实现对数据的操作
 * 返回值统统是String类型的返回页面有统一的安排方便代码提取
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@Controller
@SessionAttributes("chatRecordForm")
@RequestMapping("/chatRecord")
public class ChatRecordController extends BaseController {
    /**
     * 实体名称
     */
    private static final String ENTITYNAME = "chatRecord";

    @Resource
    private ChatRecordService chatRecordService;
    @Resource
    private RedisService redisService;

    /**
     * 初始化
     *
     * @param map            参数
     * @param chatRecordForm 条件参数
     */
    @ModelAttribute
    public void init(Model map, @ModelAttribute ChatRecordForm chatRecordForm) {
        ChatRecordForm newChatRecordForm = new ChatRecordForm();
        if (null != chatRecordForm) {
            newChatRecordForm = chatRecordForm;
        }
        map.addAttribute(String.format(Constants.FORMNAME, ENTITYNAME), newChatRecordForm);
    }

    /**
     * 获取聊天记录列表
     *
     * @param map            参数
     * @param chatRecordForm 条件参数
     * @return 页面
     */
    @RequestMapping(value = "/getAll")
    @SystemControllerLog(description = "获取聊天记录列表")
    public String getAll(Model map, @ModelAttribute ChatRecordForm chatRecordForm) {
        map.addAttribute(String.format(Constants.FORMNAME, ENTITYNAME), chatRecordForm);
        Page<ChatRecord> page = new Page<>(chatRecordForm.getPageIndex(), chatRecordForm.getPageSize());
        IPage<ChatRecord> result = chatRecordService.findAll(page, chatRecordForm);
        map.addAttribute(Constants.PI, result);
        return String.format(Constants.LISTPAGE, ENTITYNAME);
    }

    /**
     * 浏览聊天记录记录
     *
     * @param map            参数
     * @param id             主键
     * @param chatRecordForm 条件参数
     * @return 页面
     */
    @GetMapping("/view")
    @SystemControllerLog(description = "浏览聊天记录")
    public String openViewPage(Model map, @RequestParam("id") Long id,
                               @ModelAttribute ChatRecordForm chatRecordForm) {
        ChatRecord chat = chatRecordService.findByKey(id);
        map.addAttribute(ENTITYNAME, chat);
        return String.format(Constants.VIEWPAGE, ENTITYNAME);
    }

}

