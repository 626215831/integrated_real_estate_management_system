package xyz.ibenben.zhongdian.common.activitiListeners;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.delegate.TaskListener;

import java.util.Arrays;

/**
 * 创建流程中名称为对应的监听事件处理办理人流程
 *
 * @author chenjian
 */
public class MyTaskHandlerListener implements TaskListener, ExecutionListener {
    @Override
    public void notify(DelegateTask delegateTask) {
        if ("create".equals(delegateTask.getEventName())) {
            if ("领导审批".equals(delegateTask.getName())) {
                String[] managers = {"manager1", "manager2", "manager3"};
                delegateTask.addCandidateGroups(Arrays.asList(managers));
            } else if ("三经理审批".equals(delegateTask.getName())) {
                delegateTask.setAssignee("manager3");
            } else if ("二经理审批".equals(delegateTask.getName())) {
                delegateTask.setAssignee("manager2");
            } else if ("一经理审批".equals(delegateTask.getName())) {
                delegateTask.setAssignee("manager1");
            }
        }
    }

    @Override
    public void notify(DelegateExecution delegateExecution) {

    }
}
