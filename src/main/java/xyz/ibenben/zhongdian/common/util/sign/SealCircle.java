package xyz.ibenben.zhongdian.common.util.sign;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class SealCircle {
    private final Integer line;
    private final Integer width;
    private final Integer height;
}
