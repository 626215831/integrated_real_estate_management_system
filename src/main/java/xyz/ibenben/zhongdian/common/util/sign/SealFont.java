package xyz.ibenben.zhongdian.common.util.sign;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class SealFont {
    private String text;
    private final String family;
    private final Integer size;
    private final Boolean bold;
    private final Double space;
    private final Integer margin;

    public String getFamily() {
        return family == null ? "宋体" : family;
    }

    public boolean getBold() {
        return bold == null || bold;
    }

    public SealFont append(String text) {
        this.text += text;
        return this;
    }
}