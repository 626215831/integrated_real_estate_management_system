package xyz.ibenben.zhongdian.common.util.example.easy;

import java.util.*;

/**
 * 给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串，判断字符串是否有效。
 * 有效字符串需满足：
 * 左括号必须用相同类型的右括号闭合。
 * 左括号必须以正确的顺序闭合。
 * 注意空字符串可被认为是有效字符串。
 * <p>
 * 示例 1:
 * 输入: "()"
 * 输出: true
 * 示例 2:
 * 输入: "()[]{}"
 * 输出: true
 * 示例 3:
 * 输入: "(]"
 * 输出: false
 * 示例 4:
 * 输入: "([)]"
 * 输出: false
 * 示例 5:
 * 输入: "{[]}"
 * 输出: true
 */
public class BracketValid {
    public static void main(String[] args) {
        BracketValid bv = new BracketValid();
        System.out.println(bv.isValid(""));
        System.out.println(bv.isValid2("{([])}"));
    }

    /**
     * 我给出的解决方案
     *
     * @param s
     * @return
     */
    public boolean isValid(String s) {
        Map<Character, Character> map = new HashMap<>(3);
        map.put('(', ')');
        map.put('[', ']');
        map.put('{', '}');
        if (s == null || s.length() == 0) {
            return true;
        }
        if (s.length() % 2 == 1) {
            return false;
        }
        List<Character> list = new ArrayList<>(s.length());
        for (char c : s.toCharArray()) {
            list.add(c);
        }

        for (int i = 0; i < list.size(); i++) {
            Character first = list.get(i);
            if (i + 1 == list.size()) {
                return false;
            }
            Character second = list.get(i + 1);
            if (null == map.get(first)) {
                continue;
            }
            if (second.equals(map.get(first))) {
                list.remove(i + 1);
                list.remove(i);
                i = -1;
            }
        }
        return list.size() == 0;
    }

    /**
     * 官方给出的解决方案
     *
     * @param s
     * @return
     */
    public boolean isValid2(String s) {
        HashMap<Character, Character> mappings = new HashMap<>(3);
        mappings.put(')', '(');
        mappings.put('}', '{');
        mappings.put(']', '[');
        // Initialize a stack to be used in the algorithm.
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            // If the current character is a closing bracket.
            if (mappings.containsKey(c)) {
                // Get the top element of the stack. If the stack is empty, set a dummy value of '#'
                char topElement = stack.empty() ? '#' : stack.pop();

                // If the mapping for this bracket doesn't match the stack's top element, return false.
                if (topElement != mappings.get(c)) {
                    return false;
                }
            } else {
                // If it was an opening bracket, push to the stack.
                stack.push(c);
            }
        }
        // If the stack still contains elements, then it is an invalid expression.
        return stack.isEmpty();
    }
}
