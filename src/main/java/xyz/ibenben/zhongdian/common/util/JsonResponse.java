package xyz.ibenben.zhongdian.common.util;

import java.util.ArrayList;
import java.util.List;

//@Data
public class JsonResponse {

    private String msg;
    private String code;
    private List<?> data;
    private Long count;


    public JsonResponse() {
        this.msg = "操作成功！";
        this.code = "0";   //layUi 返回正确响应code 为0
        data = new ArrayList<>();
        this.count = 0L;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<?> getData() {
        return data;
    }

    public void setData(List<?> data) {
        this.data = data;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
