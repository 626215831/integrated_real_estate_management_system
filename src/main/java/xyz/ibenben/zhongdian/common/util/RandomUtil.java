package xyz.ibenben.zhongdian.common.util;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 随机数util
 * 对一些需要随机生成的，比如id，随机数等等
 * 规定了其中的规则后可以使用此类来做随机数处理
 *
 * @author chenjian
 * @since 2017年10月18日下午5:17:22
 */
public class RandomUtil {
    private RandomUtil() {
    }

    /**
     * 生成随机id
     *
     * @return 返回值
     */
    public static String getRandomId(String prefix) {
        String timeMiddle = DateUtils.getCurrentTime(null, "yyyyMMddHHmmss");
        return prefix + timeMiddle + getRandomNumber();
    }

    /**
     * 生成三位随机数
     *
     * @return 返回值
     */
    private static String getRandomNumber() {
        Random r = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 3; i++) {
            sb.append((char) ('0' + r.nextInt(10)));
        }
        return sb.toString();
    }

    /**
     * 生成N位随机数
     *
     * @return 返回值
     */
    public static String getRandomNumber(int n) {
        Random r = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++) {
            sb.append((char) ('0' + r.nextInt(10)));
        }
        return sb.toString();
    }

    /**
     * 生成N位大小写字母和数字的随机组合的随机数
     *
     * @param length 参数
     * @return 返回值
     */
    public static String getRandomString(int length) {
        String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(62);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }

    /**
     * 获取随机颜色值
     *
     * @return 返回值
     */
    public static String getRandomColor() {
        String red; //红色
        String green; //绿色
        String blue; //蓝色
        //生成随机对象
        Random random = new Random();
        int max = 256;
        int min = 200;
        //生成红色颜色代码
        red = Integer.toHexString(random.nextInt(max) % (max - min + 1) + min).toUpperCase();
        //生成绿色颜色代码
        green = Integer.toHexString(random.nextInt(max) % (max - min + 1) + min).toUpperCase();
        //生成蓝色颜色代码
        blue = Integer.toHexString(random.nextInt(max) % (max - min + 1) + min).toUpperCase();
        //判断红色代码的位数
        red = red.length() == 1 ? "0" + red : red;
        //判断绿色代码的位数
        green = green.length() == 1 ? "0" + green : green;
        //判断蓝色代码的位数
        blue = blue.length() == 1 ? "0" + blue : blue;
        //生成十六进制颜色值
        return "#" + red + green + blue;
    }

    public static String calculate(List<Material> materialList, List<Food> foodList) {
        StringBuilder result = new StringBuilder();
        for (Food food : foodList) {
            result.append(food.getName());
            for (NeedMaterial need : food.getList()) {
                for (Material material : materialList) {
                    if (need.getMid().equals(material.mid)) {
                        result.append("需要材料");
                        result.append(material.getName());
                        result.append(need.getNeedCount());
                        result.append(need.getUnit()).append(", 每").append(material.getUnit()).append("价格");
                        result.append(material.getPrice().toString());
                        result.append("元, 共计花费");
                        BigDecimal price;
                        if (need.getUnit().equals(material.getUnit())) {
                            price = new BigDecimal(need.getNeedCount()).multiply(material.getPrice());
                        } else {
                            BigDecimal perUnitPrice = material.getPrice().divide(need.getChangeRate(), 4, BigDecimal.ROUND_FLOOR);
                            price = perUnitPrice.multiply(new BigDecimal(need.getNeedCount())).setScale(2, BigDecimal.ROUND_FLOOR);
                        }
                        food.setPrice(food.getPrice().add(price));
                        result.append(price.toString());
                        result.append("元,");
                    }
                }
            }
            result.append("合计");
            result.append(food.getPrice().toString());
            food.setSellPrice(food.getPrice().multiply(new BigDecimal(2)));
            result.append("元, 售价");
            result.append(food.getSellPrice().toString());
            result.append("元\n\r");
        }
        return result.toString();
    }

    public static void main(String[] args) {
        List<Material> list = new ArrayList<>();
        list.add(new Material(1L, "面粉", "公斤", new BigDecimal("4.00"), 500L));
        list.add(new Material(2L, "鸡蛋", "个", new BigDecimal("1.00"), 500L));
        list.add(new Material(3L, "白糖", "克", new BigDecimal("0.20"), 500L));
        list.add(new Material(4L, "酵母", "克", new BigDecimal("0.02"), 500L));
        List<Food> foods = new ArrayList<>();
        List<NeedMaterial> needMaterials = new ArrayList<>();
        needMaterials.add(new NeedMaterial(1L, 200L, "克", new BigDecimal(1000)));
        needMaterials.add(new NeedMaterial(2L, 10L, "个", new BigDecimal(1)));
        needMaterials.add(new NeedMaterial(3L, 200L, "克", new BigDecimal(1)));
        needMaterials.add(new NeedMaterial(4L, 50L, "克", new BigDecimal(1)));
        foods.add(new Food(1L, "面包", needMaterials, new BigDecimal(0), new BigDecimal(0)));
        needMaterials = new ArrayList<>();
        needMaterials.add(new NeedMaterial(1L, 300L, "克", new BigDecimal(1000)));
        needMaterials.add(new NeedMaterial(2L, 10L, "个", new BigDecimal(1)));
        needMaterials.add(new NeedMaterial(3L, 70L, "克", new BigDecimal(1)));
        foods.add(new Food(2L, "蛋糕", needMaterials, new BigDecimal(0), new BigDecimal(0)));
        String result = calculate(list, foods);
        System.out.println(result);
    }

    @Data
    @Builder
    static
    class Material {
        /**
         * 主键
         */
        private Long mid;
        /**
         * 材料名称
         */
        private String name;
        /**
         * 单位
         */
        private String unit;
        /**
         * 每单位的价格
         */
        private BigDecimal price;
        /**
         * 共有单位数量
         */
        private Long totalCount;
    }

    @Builder
    @Data
    static
    class NeedMaterial {
        /**
         * 材料主键
         */
        private Long mid;
        /**
         * 需要数量
         */
        private Long needCount;
        /**
         * 需要数量单位
         */
        private String unit;
        /**
         * 转化率
         */
        private BigDecimal changeRate;
    }

    @Builder
    @Data
    static
    class Food {
        /**
         * 主键
         */
        private Long fid;
        /**
         * 食物名称
         */
        private String name;
        /**
         * 需要的材料列表
         */
        private List<NeedMaterial> list;
        /**
         * 计算得出的成本价格
         */
        private BigDecimal price;
        /**
         * 售价
         */
        private BigDecimal sellPrice;
    }
}
