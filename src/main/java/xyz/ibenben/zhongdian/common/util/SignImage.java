package xyz.ibenben.zhongdian.common.util;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;

public class SignImage {

    /**
     * @param name        String 名字
     * @param companyName String 公司名称
     * @param pngName     String png图片名
     * @return 结果
     */
    public static boolean createSignTextImg(String name, String companyName, String pngName) {
        try {
            int canvasWidth = 1000;
            int canvasHeight = 1000;
            //TYPE_INT_ARGB表示一个图像，它具有合成整数像素的8位RGBA颜色分量
            BufferedImage bi = new BufferedImage(canvasWidth, canvasHeight, BufferedImage.TYPE_INT_ARGB);
            //创建一个GraphicsD，可以将它绘制到此BufferedImage中
            Graphics2D g2 = bi.createGraphics();
            //画笔
            //圆形半径
            int circleRadius = canvasWidth / 2 - 15;
            //画圆
            g2.setPaint(Color.RED);
            g2.setStroke(new BasicStroke(15));
            //设置画笔粗细/Arc2D.Double(指定的位置，大小，角跨越，闭合类型);
            Shape circle = new Arc2D.Double(15, 15, circleRadius * 2, circleRadius * 2, 0, 360, Arc2D.OPEN);
            g2.draw(circle);
            //设置样式
            int fontSize = 30;
            //设置字体样式
            Font f = new Font("宋体", Font.BOLD, fontSize);
            //获取font呈现的上下文
            g2.setFont(f);
            fontSize = canvasWidth / 10;
            f = new Font("黑体", Font.BOLD, fontSize);
            FontRenderContext context = g2.getFontRenderContext();
            Rectangle2D bounds = f.getStringBounds(companyName, context);
            g2.setFont(f);
            g2.drawString(companyName, (float) (circleRadius - bounds.getCenterX()), (float) (circleRadius * 1.7 - bounds.getCenterY()));
            //center
            Font starFont = new Font("宋体", Font.BOLD, canvasWidth / 2);
            g2.setFont(starFont);
            g2.drawString("★", canvasWidth / 4, canvasHeight - canvasHeight / 3);
            //head
            fontSize = canvasWidth / 10;
            f = new Font("宋体", Font.BOLD, fontSize);
            bounds = f.getStringBounds(name, context);
            double msgWidth = bounds.getWidth();
            int countOfMsg = name.length();
            //计算间距
            double interval = msgWidth / (countOfMsg - 2) - 20;
            //bounds.getY()是负数，这样可以将弧形文字固定在圆内了。-5目的是离圆环稍远一点
            double newRadius = circleRadius + bounds.getY() - 55;
            //每个间距对应的角度
            double radianPerInterval = 2 * Math.asin(interval / (2 * newRadius));
            //第一个元素的角度
            double firstAngle;
            if (countOfMsg % 2 == 1) {
                //奇数
                firstAngle = (countOfMsg - 1) * radianPerInterval / 2.0 + Math.PI / 2 + 0.08;
            } else {
                //偶数
                firstAngle = (countOfMsg / 2.0 - 1) * radianPerInterval + radianPerInterval / 2.0 + Math.PI / 2 + 0.08;
            }

            for (int i = 0; i < countOfMsg; i++) {
                double aa = firstAngle - i * radianPerInterval;
                //小小的trick，将【0，pi】区间变换到[pi/2,-pi/2]区间
                double ax = newRadius * Math.sin(Math.PI / 2 - aa);
                //同上类似，这样处理就不必再考虑正负的问题了
                double ay = newRadius * Math.cos(aa - Math.PI / 2);
                AffineTransform transform = AffineTransform.getRotateInstance(Math.PI / 2 - aa);
                Font f2 = f.deriveFont(transform);
                g2.setFont(f2);
                g2.drawString(name.substring(i, i + 1), (float) (circleRadius + ax + 15), (float) (circleRadius - ay));
            }
            //销毁资源
            g2.dispose();
            // 指定输出文件
            ImageIO.write(bi, "PNG", new File(pngName));
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public static void main(String[] args) {
        SignImage.createSignTextImg("北京至臻云智能科技有限公司", "财务专用章", "d:\\sign.png");
    }
}
