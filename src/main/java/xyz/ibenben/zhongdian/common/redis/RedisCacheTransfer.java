package xyz.ibenben.zhongdian.common.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.stereotype.Component;

/**
 * @author chenjian
 * @since 2017年10月18日下午5:17:22
 */
@Component
public class RedisCacheTransfer {

    /**
     * 设置redis连接工厂
     *
     * @param jedisConnectionFactory
     */
    @Autowired
    public void setJedisConnectionFactory(
            RedisConnectionFactory jedisConnectionFactory) {
        MybatisRedisCache.setRedisConnectionFactory(jedisConnectionFactory);
    }
}
