package xyz.ibenben.zhongdian.common.exception;

import lombok.Getter;

/**
 * @author chenjian
 * @since 2017年10月18日下午5:17:22
 */
@Getter
public enum ExceptionEnum {
    //未知错误
    UNKONW_ERROR(-1, "未知错误"),
    //成功
    SUCCESS(0, "成功"),
    //无法创建文件夹
    NOFILEEXCEPTION(100, "无法创建文件夹"),
    //无法获取输入流异常
    INPUTEXCEPTION(101, "无法获取输入流异常"),
    //无法获取输出流异常
    OUTPUTEXCEPTION(102, "无法获取输出流异常"),
    //无法删除文件
    NODELETEEXCEPTION(103, "无法删除文件"),
    //没有发现该类异常
    NOCLASSEXCEPTION(104, "没有发现该类异常"),
    //发送邮件异常
    SENDEMAILEXCEPTION(105, "发送邮件异常"),
    //无法获取邮件模板异常
    NOEMAILMODELEXCEPTION(106, "无法获取邮件模板异常"),
    //处理模板异常
    PROCESSMODELEXCEPTION(107, "处理模板异常"),
    //编码异常
    CODEEXCEPTION(108, "编码异常"),
    //转换时间出错
    CONVERTTIMEEXCEPTION(109, "转换时间出错"),
    //系统错误
    SYSTEMERROREXCEPTION(110, "系统错误"),
    //文件获取失败
    CONVERTFILEEXCEPTION(111, "文件获取失败"),
    //获取session factory异常
    SESSIONFACTORYEXCEPTION(112, "获取session factory异常"),
    //SQL查询异常
    SQLEXCEPTION(113, "SQL查询异常"),
    //反序列化失败异常
    DESERIALIZEEXCEPTION(114, "反序列化失败异常"),
    //Cache instances require an ID
    NEEDIDEXCEPTION(115, "Cache instances require an ID"),
    //JAVA反射异常
    NOCONSTRUCTOREXCEPTION(116, "JAVA反射异常"),
    //安全权限异常
    SAFEEXCEPTION(117, "安全权限异常"),
    //引包异常
    JAREXCEPTION(118, "引包异常"),
    //定时任务异常
    TASKEXCEPTION(119, "定时任务异常"),
    //用户已经被锁定不能登录，请与管理员联系！
    LUCKUSEREXCEPTION(201, "用户已经被锁定不能登录，请与管理员联系！"),
    //用户名或密码错误次数大于5次,账户已锁定
    PASSWORDERROREXCEPTION(202, "用户名或密码错误次数大于5次,账户已锁定"),
    //用户名或密码不正确
    USERORPASSWORDERROREXCEPTION(203, "用户名或密码不正确"),
    //用户已经被锁定不能登录，请与管理员联系！
    LUCKUSEREXCEPTION2(204, "用户已经被锁定不能登录，请与管理员联系！"),
    //用户名或密码错误次数大于5次,账户已锁定
    PASSWORDERROREXCEPTION2(205, "用户名或密码错误次数大于5次,账户已锁定"),
    //用户名或密码不正确
    USERORPASSWORDERROREXCEPTION2(206, "用户名或密码不正确"),
    //一小时后解除限制，请稍后
    PROCESSUSERLIMITEXCEPTION(300, "一小时后解除限制，请稍后");

    /* 代码 */
    private final Integer code;

    /* 信息 */
    private final String msg;

    /**
     * 构造方法
     *
     * @param code 参数
     * @param msg  参数
     */
    ExceptionEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
