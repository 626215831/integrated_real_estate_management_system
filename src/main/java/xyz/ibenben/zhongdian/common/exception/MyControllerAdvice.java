package xyz.ibenben.zhongdian.common.exception;

import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import xyz.ibenben.zhongdian.common.constants.Constants;

/**
 * 统一异常管理类
 * 系统内的所有异常通过MyException类来收集，并由此类来控制转发或跳转、重定向等操作
 *
 * @author chenjian
 * @since 2017年10月18日下午5:17:22
 */
@ControllerAdvice
public class MyControllerAdvice {
    /**
     * 统一异常管理
     *
     * @param ex 参数
     * @return 返回值
     */
    @ExceptionHandler(value = MyException.class)
    public ModelAndView myErrorHandler(MyException ex) {
        ModelAndView modelAndView = new ModelAndView();
        //当枚举内的数据取出来等于2开头的需要分类跳转到不通页面
        if (ex.getCode().equals(201) || ex.getCode().equals(202) || ex.getCode().equals(203)) {
            modelAndView.setViewName("login/emailActivateFailure");
        } else if (ex.getCode().equals(204) || ex.getCode().equals(205) || ex.getCode().equals(206)) {
            modelAndView.setViewName("login/login");
        } else if (ex.getCode().equals(300)) {
            throw new ExcessiveAttemptsException();
        } else {
            //其他的一律跳转到404页面
            modelAndView.setViewName("error/404");
        }
        //并为页面提供异常Code和异常消息
        modelAndView.addObject(Constants.CODE, ex.getCode());
        modelAndView.addObject(Constants.MSG, ex.getMsg());
        return modelAndView;
    }

}
