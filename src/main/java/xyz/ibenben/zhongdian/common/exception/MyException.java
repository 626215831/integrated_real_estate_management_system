package xyz.ibenben.zhongdian.common.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * @author chenjian
 * @since 2017年10月18日下午5:17:22
 */
@Setter
@Getter
public class MyException extends RuntimeException {
    protected final Integer code;
    protected final String msg;
    protected final Exception e;
    protected final Long userId;

    /**
     * 构造函数
     *
     * @param enums 参数
     * @param e     参数
     */
    public MyException(ExceptionEnum enums, Exception e) {
        this.code = enums.getCode();
        this.msg = enums.getMsg();
        this.e = e;
        this.userId = 0L;
    }

    /**
     * 构造函数
     *
     * @param enums 参数
     * @param e     参数
     */
    public MyException(ExceptionEnum enums, Exception e, Long userId) {
        this.code = enums.getCode();
        this.msg = enums.getMsg();
        this.e = e;
        this.userId = userId;
    }

}
