package xyz.ibenben.zhongdian.common.configure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import xyz.ibenben.zhongdian.common.intercepter.MyInterceptor;

/**
 * 配置Web
 *
 * @author chenjian
 * @since 2017年10月18日下午5:17:22
 */
@Configuration
public class MyWebAppConfigurer implements WebMvcConfigurer {
    /**
     * 此方法把该拦截器实例化成一个bean,否则在拦截器里无法注入其它bean
     *
     * @return 拦截器
     */
    @Bean
    MyInterceptor myInterceptor() {
        return new MyInterceptor();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("/process/**").addResourceLocations("classpath:/process/");
        registry.addResourceHandler("/static/**/**/**/**").addResourceLocations("classpath:/static/**/**/**/");
    }

}
