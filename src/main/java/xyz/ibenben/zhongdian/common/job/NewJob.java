package xyz.ibenben.zhongdian.common.job;

import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import xyz.ibenben.zhongdian.system.entity.sys.SysUser;
import xyz.ibenben.zhongdian.system.service.sys.SysUserService;

import java.util.Date;

/**
 * 定时任务类
 * 实现了定时任务的基类
 * 并需要实现其父类里的方法
 *
 * @author chenjian
 * @since 2017年10月18日下午5:17:22
 */
@Slf4j
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class NewJob implements Job {

    /**
     * 解锁1小时后数据库内的数据
     *
     * @param context 内容
     */
    @Override
    public void execute(JobExecutionContext context) {
        log.info("New Job执行时间: " + new Date());
        JobDataMap map = context.getMergedJobDataMap();
        if (map != null && !map.isEmpty()) {
            //把数据库里的enable设置打开，让用户可以重新登录
            SysUserService sysUserService = (SysUserService) map.get("sysUserService");
            Long userId = Long.parseLong(map.get("userId").toString());
            SysUser user = sysUserService.getById(userId);
            user.setEnable(1);
            sysUserService.updateById(user);
        }
    }

}