package xyz.ibenben.zhongdian.common.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * @author chenjian
 * @since 2017年10月18日下午5:17:22
 */
public class Constants {
    /**
     * page
     */
    public static final String PI = "pi";
    /**
     * 逗号
     */
    public static final String COMMA = ",";
    /**
     * 主键
     */
    public static final String ID = "id";
    /**
     * OK
     */
    public static final String OK = "ok";
    /**
     * 错误
     */
    public static final String ERROR = "error";
    /**
     * 加密类型
     */
    public static final String ALGORITHMNAME = "md5";
    /**
     * 加密次数
     */
    public static final int HASHITERATIONS = 2;
    /**
     * Session里的用户主键
     */
    public static final String SESSIONID = "userSessionId";
    /**
     * Session里的用户
     */
    public static final String SESSION = "userSession";
    /**
     * 成功
     */
    public static final String SUCCESS = "success";
    /**
     * 消息
     */
    public static final String MSG = "msg";
    /**
     * 代码
     */
    public static final String CODE = "code";
    /**
     * 查询条件名称
     */
    public static final String FORMNAME = "%sForm";
    /**
     * 列表名称
     */
    public static final String LISTENTITY = "%sList";
    /**
     * 列表页面
     */
    public static final String LISTPAGE = "%s/list";
    /**
     * 重定向页面
     */
    public static final String REDIRECT = "redirect:/%s/getAll";
    /**
     * 添加页面
     */
    public static final String ADDPAGE = "%s/add";
    /**
     * 编辑页面
     */
    public static final String EDITPAGE = "%s/edit";
    /**
     * 查看页面
     */
    public static final String VIEWPAGE = "%s/view";
    /**
     * 上传页面
     */
    public static final String UPLOADPAGE = "%s/upload";

    /**
     * 数据库
     */
    public static final String INDEXNAME = "zhongdian";

    private Constants() {
    }

    /**
     * 根据表名获取对应的excel表头
     *
     * @param tableName 参数
     * @return 返回值
     */
    public static Map<String, String> getMap(String tableName) {
        Map<String, String> map = new HashMap<>();
        if ("houseInfo".equals(tableName)) {
            map.put("序号", "id");
            map.put("房屋名称", "houseName");
            map.put("室数量", "roomNumber");
            map.put("厅数量", "drawingNumber");
            map.put("厨数量", "kitchenNumber");
            map.put("卫数量", "restroomNumber");
            map.put("朝向", "orientation");
            map.put("房屋面积", "houseArea");
            map.put("所在楼层", "layer");
            map.put("总楼层", "totalLayer");
            map.put("所在小区", "polt");
            map.put("详细地址", "address");
            map.put("所在省", "province");
            map.put("所在市", "city");
            map.put("所在区", "region");
            map.put("住宅用途", "purpose");
            map.put("购买时间", "buyTime");
            map.put("产权", "propertyRight");
            map.put("类型", "type");
        }
        return map;
    }

}
