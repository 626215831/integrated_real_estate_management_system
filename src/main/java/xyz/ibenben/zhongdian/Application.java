package xyz.ibenben.zhongdian;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 配置 sources 和启动项目
 * springboot的启动注解
 * 开启事物管理的注解
 * 自动扫描包注解
 * Mapper扫描包注解
 * 添加这些注解后springboot会自动加载一些默认的设置，以免程序有过多的配置文件导致程序的可读性变差
 *
 * @author chenjian
 * @since 2017年9月27日上午10:57:28
 */
@SpringBootApplication(exclude = {
        ManagementWebSecurityAutoConfiguration.class,
        SecurityAutoConfiguration.class,
})
@EnableTransactionManagement
@EnableScheduling
@ServletComponentScan
@MapperScan(basePackages = {"xyz.ibenben.zhongdian.system.mapper.sys", "xyz.ibenben.zhongdian.system.mapper"})
public class Application extends SpringBootServletInitializer {
    /**
     * 启动方法
     *
     * @param args 无用参数
     */
    public static void main(String[] args) {
        System.setProperty("es.set.netty.runtime.available.processors", "false");
        SpringApplication.run(Application.class, args);
    }

    /**
     * 配置
     *
     * @param application 参数
     * @return 返回值
     */
    @Override
    protected SpringApplicationBuilder configure(
            SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

}