<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>激活并认证</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <style type="text/css">
        a.addHover:hover {
            text-decoration: underline !important;
        }
    </style>
</head>

<body style="margin: 0; padding: 0;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">

    <tr>
        <td style="padding: 10px 0 30px 0;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700"
                   style="border: 1px solid #cccccc; border-collapse: collapse;">
                <tr>
                    <td>
                        <img style="border: 0;" src="${title_image}" width="700"/>
                    </td>
                </tr>
                <tr>
                    <td style="height: 80px;padding: 50px 60px 0px 40px;font-size:14px;  line-height: 25px;font-family: '微软雅黑';">
                        <b>亲爱的：</b><b>${username}</b>，
                        <br>
                        <br><b>房屋管理系统小助理小E温馨提醒：您距离获取房屋信息的贴心服务等功能仅差一步。</b>
                        <br>
                        <div style="text-align:center; padding-top:16px;">
                            <a href="${url}"
                               style="color:#fff; background:#ed7800; padding:0px 12px;text-decoration: none; display: inline-block; ">激活并完成认证</a>
                        </div>

                    </td>
                </tr>
                <tr>
                    <td style="height: 100px;padding: 10px 60px 0px 40px;font-size:14px;  line-height: 25px;font-family: '微软雅黑';">
                        <b>如果点击按钮没有反应，可点击下面的链接或复制下面的链接到浏览器地址栏中完成激活和进行下一步：</b><br>
                        <a class="addHover" href="${url}" style="color:#1470cc;text-decoration: none;">${url}</a>
                    </td>
                </tr>
                <tr>
                    <td style="height: 100px;padding: 10px 60px 0px 40px;font-size:14px;  line-height: 25px;font-family: '微软雅黑';">
                        <b>感谢您选择房屋管理系统。 <br>
                            感谢您的配合，祝您生意兴隆！
                            <br>房屋管理系统交易平台</b>
                    </td>
                </tr>
                <tr>
                    <td style="line-height: 24px;padding: 0px 60px 50px 40px;font-size:14px; font-weight: bold;font-family: '微软雅黑';">
                        <br>优配全球房屋资源！
                        <br>Copyright@Qingdao House Technology Co., Ltd 2015-2017
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#ED7800"
                        style="height: 30px;color: white;text-align:center;font-size: 24px;font-weight: bold;">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>

</html>