<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>重置密码</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <style type="text/css">
        a.addHover:hover {
            text-decoration: underline !important;
        }
    </style>
</head>
<body style="margin: 0; padding: 0;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">

    <tr>
        <td style="padding: 10px 0 30px 0;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700"
                   style="border: 1px solid #cccccc; border-collapse: collapse;">
                <tr>
                    <td>
                        <img style="border: 0;" src="${title_image}" width="700"/>
                    </td>
                </tr>
                <tr>
                    <td style="height: 80px;padding: 50px 60px 0px 40px;font-size:14px;  line-height: 25px;font-family: '微软雅黑';">
                        <b>亲爱的：</b><b>${username}</b>，
                        <br>
                        <br><b>欢迎使用密码找回功能</b>
                        <br>

                    </td>
                </tr>
                <tr>
                    <td style="height: 80px;padding: 10px 60px 0px 40px;font-size:14px;  line-height: 25px;font-family: '微软雅黑';">
                        <b>房屋管理系统小助理小E提示您本次验证码是：<span style="font-size: 18px;color: #ed7800;">${content}</span>
                            请在60分钟以内在找回密码页或更改邮箱页填入此验证码。如果您未进行相关操作，可能其他用户误输您的电子邮箱，请忽略该邮件。</b>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 10px 60px 0px 40px;font-size:14px;  line-height: 25px;font-family: '微软雅黑';">
                        <b>（请注意，该电子邮件未支持接收回复邮件，如有疑问请直接访问 <a class="addHover" style="color: #ed7800;" href="${index}">房屋管理系统平台。</a>）</b>
                    </td>
                </tr>
                <tr>
                    <td style="height: 100px;padding: 10px 60px 0px 40px;font-size:14px;  line-height: 25px;font-family: '微软雅黑';">
                        <b>感谢您选择房屋管理系统。 <br>
                            感谢您的配合，祝您生意兴隆！
                            <br>房屋管理系统交易平台</b>
                    </td>
                </tr>
                <tr>
                    <td style="line-height: 24px;padding: 0px 60px 50px 40px;font-size:14px; font-weight: bold;font-family: '微软雅黑';">
                        <br>优配全球房屋资源！
                        <br>Copyright@Qingdao House Technology Co., Ltd 2015-2017
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#ED7800"
                        style="height: 30px;color: white;text-align:center;font-size: 24px;font-weight: bold;">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>