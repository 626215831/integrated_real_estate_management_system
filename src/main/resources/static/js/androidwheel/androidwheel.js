$(function () {
    initCountry($("#province").val(), $("#city").val(), $("#region").val(), $("#province").find("option:selected").text());
});

//省份变更
function provinceChange(provinceCode, cityCode, text) {
    if (provinceCode == null || provinceCode == "") {
        //市清空
        var cityInfo = new Array();
        cityInfo.push({"text": text, "value": ""});
        $("#city").empty();
        $.each(cityInfo, function (a, b) {
            $("#city").append("<option value='" + b.value + "'>" + b.text + "</option>");
        });

        //区清空
        var regionInfo = new Array();
        regionInfo.push({"text": text, "value": ""});
        $("#region").empty();
        $.each(regionInfo, function (a, b) {
            $("#region").append("<option value='" + b.value + "'>" + b.text + "</option>");
        });
    } else {
        $.ajax({
            url: "/houseInfo/getCityList",
            type: "post",
            data: "provinceCode=" + provinceCode,
            dataType: "json",
            success: function (data) {
                var cityList = eval(data);

                var cityInfo = [];
                cityInfo.push({"text": text, "value": ""});
                for (var i = 0; i < cityList.length; i++) {
                    cityInfo.push({"text": cityList[i].cityName, "value": cityList[i].cityCode});
                }

                $("#city").empty();
                $.each(cityInfo, function (a, b) {
                    var city = "<option value='" + b.value + "' " + (b.value == cityCode ? "selected='selected'" : "") + ">" + b.text + "</option>";
                    $("#city").append(city);
                });

//				var regionInfo = [];
//				regionInfo.push({ "text": text, "value": "" });
//				$("#region").empty();
//				$.each(regionInfo,function(a,b){
//					$("#region").append("<option value='"+b.value+"'>"+b.text+"</option>");
//				});
            }
        });
    }
}

//城市变更
function cityChange(cityCode, regionCode, text) {
    if (cityCode == null || cityCode == "") {
        //区清空
        var regionInfo = [];
        regionInfo.push({"text": text, "value": ""});
        $("#region").empty();
        $.each(regionInfo, function (a, b) {
            $("#region").append("<option value='" + b.value + "'>" + b.text + "</option>");
        });
    } else {
        $.ajax({
            url: "/houseInfo/getRegionList",
            type: "post",
            data: "cityCode=" + cityCode,
            dataType: "json",
            success: function (data) {
                var regionList = data;
                var regionInfo = [];
                regionInfo.push({"text": text, "value": ""});
                for (var i = 0; i < regionList.length; i++) {
                    regionInfo.push({"text": regionList[i].regionName, "value": regionList[i].regionCode});
                }
                $("#region").empty();
                $.each(regionInfo, function (a, b) {
                    var region = "<option value='" + b.value + "' " + (b.value == regionCode ? "selected='selected'" : "") + ">" + b.text + "</option>";
                    $("#region").append(region);
                });
            }
        });
    }
}

function initCountry(provinceCode, cityCode, regionCode, text) {
    provinceChange(provinceCode, cityCode, text);
    cityChange(cityCode, regionCode, text);
}