// 初始化 ws 对象
var host;

$(function () {
    $('.tab-pane').slimScroll({//设置模块出现滚动条
        height: '600px'
    });

    $('#publicscroll').slimScroll({scrollTo: ($('#publicscroll').height()) + 'px'});
    $('#privatescroll').slimScroll({scrollTo: ($('#privatescroll').height()) + 'px'});
    host = $("#hosts").val();

    client = Stomp.client('ws://' + host + ':15674/ws');
    // 定义连接成功回调函数
    var on_connect = function (x) {
        //data.body是接收到的数据
        client.subscribe("/exchange/spring-boot-exchange/spring-boot-routingKey", function (data) {
            var msg = JSON.parse(data.body);
            //分类型展示
            if (msg.type === 1) {
                //淡入淡出效果
                $("#text").fadeIn(3000);
                $("#text").text("欢迎" + msg.username + "登录房屋管理系统");
                $("#text").fadeOut(3000);
            } else if (msg.type === 2) {
                appendData(msg.type, msg.record.backColor, msg.record.fontColor, msg.record.fromId, msg.record.fromUserName, msg.record.toUserName, msg.record.content, msg.record.head, false);
            } else if (msg.type === 3) {
                if ($("#fromId").val() == msg.record.fromId || $("#fromId").val() == msg.record.toId) {
                    appendData(msg.type, msg.record.backColor, msg.record.fontColor, msg.record.fromId, msg.record.fromUserName, msg.record.toUserName, msg.record.content, msg.record.head, false);
                }
                if ($("#toId").val() == msg.record.fromId || $("#toId").val() == msg.record.toId) {
                    appendData(msg.type, msg.record.backColor, msg.record.fontColor, msg.record.fromId, msg.record.fromUserName, msg.record.toUserName, msg.record.content, msg.record.head, true);
                }
            }
        });
    };
    // 定义错误时回调函数
    var on_error = function () {
        //console.log('error');
    };

    // 连接RabbitMQ
    client.connect('admin', 'admin', on_connect, on_error, '/');
});

$("#send").on("click", function () {
    sendMessage(1, null, $("#publiccontent").val());
});
$('#publiccontent').bind('keypress', function (event) {
    if (event.keyCode === "13") {
        sendMessage(1, null, $(this).val());
    }
});

function sendMessage(type, toId, content) {
    if (type === 2 && toId === '') {
        return;
    }
    $.ajax({
        url: '/sendMessage',
        type: "post",
        data: {fromId: $("#fromId").val(), toId: toId, type: type, content: content},
        dataType: "json",
        success: function (data) {
            if (!data.success) {
                if (type === 1) {
                    $("#publiccontent").attr('placeholder', data.msg);
                } else {
                    $("#privatecontent").attr('placeholder', data.msg);
                }
            } else {
                if (type === 1) {
                    $("#publiccontent").val('');
                } else {
                    $("#privatecontent").val('');
                }
            }
        }
    });
}

$("#sendprivate").on("click", function () {
    sendMessage(2, $("#toId").val(), $("#privatecontent").val());
});
$('#privatecontent').bind('keypress', function (event) {
    if (event.keyCode === "13") {
        sendMessage(2, $("#toId").val(), $(this).val());
    }
});

function getPrivateRecord(value, type) {
    $("#privatetext").empty();
    if (value != null && value !== '') {
        $.ajax({
            url: '/getMessage',
            type: "post",
            data: {fromId: $("#fromId").val(), toId: value, type: type},
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    var msg = data.obj.messages;
                    for (var i = 0; i < msg.length; i++) {
                        appendData(3, msg[i].backColor, msg[i].fontColor, msg[i].fromId, msg[i].fromUserName, msg[i].toUserName, msg[i].content, msg[i].head, true);
                    }
                }
            }
        });
    }
}

function appendData(type, backColor, fontColor, fromId, fromUserName, toUserName, content, head, isPrivate) {
    var headtitle = "public";
    if (isPrivate) {
        headtitle = "private"
    }
    if (type !== 2) {
        fromUserName = fromUserName + "私聊" + toUserName;
    }
    var str = "<div class='itemdiv dialogdiv'><div class='user " + ($("#fromId").val() == fromId ? "userright" : "") + "'>" +
        "<img alt='" + fromUserName + "' src='" + (head === "" ? "/images/user.jpg" : "/" + head) + "' style='max-height: 50px;max-width: 50px;'/>" +
        "</div><div class='body " + ($("#fromId").val() == fromId ? "right" : "") + "' style='margin-right:50px; background-color:" + backColor +
        ";color:" + fontColor + ";'>" +
        "<div class='name'><a href='#'>" + fromUserName + "</a></div><div class='text'>" + content + "</div></div></div>";
    $("#" + headtitle + "text").append(str);
    $("#" + headtitle + "scroll").slimScroll({scrollTo: ($('#' + headtitle + 'scroll').height()) + 'px'});
}