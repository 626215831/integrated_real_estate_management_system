//弹出选择角色的框
var setting = {
    view: {
        showIcon: false,
        addDiyDom: addDiyDom
    },
    check: {
        enable: true,
        chkboxType: {"Y": "ps", "N": "s"}
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "id",
            pIdKey: "PId"
        }
    }
};

var zNodes = eval($("#resourcesList").val());
$(document).ready(function () {
    $.fn.zTree.init($("#treeDemo"), setting, zNodes);
    var zTree = $.fn.zTree.getZTreeObj("treeDemo");
    zTree.expandAll(true);
});


function addDiyDom(treeId, treeNode) {
    if (treeNode.isLeaf === false) {
        $("#" + treeNode.tId).addClass("isLeaf");
    }
}

//保存权限的选择
$("form").submit(function () {
    var zTree = $.fn.zTree.getZTreeObj("treeDemo"), checkNode = zTree.getCheckedNodes(true);
    var ids = new Array();
    for (var i = 0; i < checkNode.length; i++) {
        ids.push(checkNode[i].id);
    }
    $("#resourcesId").val(ids.join(","));
    $(":submit", this).attr("disabled", "disabled");
}); 