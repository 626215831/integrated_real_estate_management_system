# 房地产综合管理系统
`house.sql是数据库文件，对应配置信息修改数据库名称,
 系统需要的工具还有Redis，RabbitMQ，MySQL，Mongodb，Elasticsearch（缺少哪一个都不能运行）`
#### 本项目使用SpringBoot+Mybits+MySQL开发

###### 1、分页使用的是pageHelper
###### 2、实体类使用了lombok来处理Setter、Getter、ToString、LOG等
###### 3、Dao层使用的是tk.mybatis.mapper.common.Mapper和tk.mybatis.mapper.common.MySqlMapper
#### 使用了Shiro来处理权限管理
###### 1、基本权限管理，配合数据库的五张表实现
###### 2、处理Remember Me的配置及逻辑实现
#### 使用了FreeMarker模板发送邮件
###### 1、对注册用户里的激活账户和找回密码等功能实现发送邮件来完成验证
###### 2、在往后的版本修改中会对邮件的token做修正
#### 使用了Redis来处理缓存（shiro<del>和mybits</del>）
###### 1、使用Redis来处理Shiro的缓存
###### 2、<del>使用Redis来处理Mybits的二级缓存</del>
#### 使用了Spring AOP技术处理日志的收集
###### 1、新建切面的注解用来收集日志
###### 2、通过AOP实现对方法的切面处理
#### 使用了RabbitMQ实现用户聊天机制（待完善）
###### 1、聊天室雏形有待完善
#### 使用了Mongodb代替原来的三级联动保存数据
###### 1、省市区三级联动由原来的Mysql替换到Mongodb中保存
###### 2、初始化时通过Mongodb放入Redis中
#### 使用了Elasticsearch数据库来存储房屋信息
###### 1、房屋信息存储到NOSQL中以便以后快速查找和统计
###### 2、同步数据从MySql到Elasticsearch中（待完善）
#### 使用了Quartz实现定时任务
#### 使用了Enum枚举处理字符串与数字对应数据库记录
#### 使用了统一异常管理技术
#### 使用了系统预加载技术

Maven的使用
======

1、需要本地安装Maven（[Maven安装](https://www.cnblogs.com/eagle6688/p/7838224.html)）

2、需要Maven编译项目，具体的编译方法：

+ 使用DOS或者Linux，首先需要跳到项目路径下，运行以下命令编译项目

    mvn compile

    Maven会自动编译项目，编译完成的.class文件将会出现在target/classes目录下

+ 如果不想直接运行.class文件，可以将其打包：

    mvn package 

    打包完成后，会在target目录下生成一个JAR文件，文件名由`<artifactId>` 和 `<version>`组成。

    把此文件复制到tomcat对应的目录下，运行tomcat也可以实现同样的效果

+ 编译完成后即可使用idea或eclipse等直接运行，也可以通过容器把打包好的文件放入容器内运行，
类似于放置在tomcat里的/CATALINA_HOME/webapps/里，然后到/bin目录下运行startup.bat（for Windowns）
或startup.sh（for Linux）

#### 建议使用idea开发项目，eclipse没试过，可能会报错

![效果图](https://gitee.com/cj5586/integrated_real_estate_management_system/raw/master/src/main/resources/static/images/show/zhongdian_show_index.png)

![效果图](https://gitee.com/cj5586/integrated_real_estate_management_system/raw/master/src/main/resources/static/images/show/zhongdian_show_info.png)

![效果图](https://gitee.com/cj5586/integrated_real_estate_management_system/raw/master/src/main/resources/static/images/show/zhongdian_show_photo.png)

![效果图](https://gitee.com/cj5586/integrated_real_estate_management_system/raw/master/src/main/resources/static/images/show/zhongdian_show_resource.png)

![效果图](https://gitee.com/cj5586/integrated_real_estate_management_system/raw/master/src/main/resources/static/images/show/zhongdian_show_rule.png)

![效果图](https://gitee.com/cj5586/integrated_real_estate_management_system/raw/master/src/main/resources/static/images/show/zhongdian_show_user.png)
#### 作者的联系方式：
###### 邮箱：cj5586@qq.com
###### 微信：cj5586